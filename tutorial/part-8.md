Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 8: Fourier Transforms & Frequency-Domain Doppler Shifting

So far our sound has always been represented the same way, as a series of sampled values that draw the shape of the waveform. If you were to picture this as a graph the X axis would be time, advancing from the left to right and the Y axis would be intensity, increasing in both positive and negative directions away from the zero line. We've looked at that graph before and it looks like this:

![](res/lpcm.png)

In this graph frequency is implicit. You can see the frequency being drawn by the samples and, if the sound is simple enough (like the one pictured above), you can measure between the peaks to figure out what the frequency must be. But there's no axis labeled frequency. This type of graph is called the [time domain](https://en.wikipedia.org/wiki/Time_domain), because it graphs how the intensity varies over time. There is however another option: a graph that places frequency on the X axis and intensity on the Y. This is known as the [frequency domain](https://en.wikipedia.org/wiki/Frequency_domain) because it graphs how intensity varies with frequency. That graph might look like this:

![](res/frequency-domain.png)

Note the way the graph is mirrored, we'll come back to that later. This graph isn't just a representational thing for you to look at: it's a real thing we can do to the numbers representing our audio and doing so allows us to easily perform operations on that audio that would be harder in the time domain. To convert between the time domain and the frequency domain we use an operation called the [Fourier transform](https://en.wikipedia.org/wiki/Fourier_transform). The Fourier transform takes us from the time domain to the frequency domain while the inverse Fourier transform takes us in the opposite direction. The mathematically ideal Fourier transform operates on a continuous waveform and produces a continuous frequency distribution as a result. This means there are no samples in the input, just a waveform with infinite resolution. We can't do this on a computer so instead we'll be implementing the [discrete Fourier transform](https://en.wikipedia.org/wiki/Discrete_Fourier_transform) which operates on a waveform represented by an array of discrete samples and produces an array of frequency "buckets" as an output. More specifically we'll be using the [fast Fourier transform](https://en.wikipedia.org/wiki/Fast_Fourier_transform) algorithm, a popular way of implementing the discrete Fourier transform in practice.

The implementation of the fast Fourier transform (called FFT and IFFT for the inverse from here on) looks like this:

```
inline void fastFourierTransform(valarray<complex<float>>& x)
{
    size_t n = x.size();

    if (n <= 1)
    {
        return;
    }

    valarray<complex<float>> even = x[slice(0, n / 2, 2)];
    valarray<complex<float>> odd = x[slice(1, n / 2, 2)];

    fastFourierTransform(even);
    fastFourierTransform(odd);

    for (size_t k = 0; k < n / 2; k++)
    {
        complex<float> t = polar<float>(1, -2 * pi * k / n) * odd[k];

        x[k] = even[k] + t;
        x[k + (n / 2)] = even[k] - t;
    }
}

inline valarray<complex<float>> fft(const vector<float>& samples)
{
    assert(samples.size() != 0 && (samples.size() & (samples.size() - 1)) == 0);

    valarray<complex<float>> x(samples.size());

    for (size_t i = 0; i < samples.size(); i++)
    {
        x[i] = samples[i];
    }

    fastFourierTransform(x);

    return x;
}

inline vector<float> ifft(valarray<complex<float>> x)
{
    for (complex<float>& v : x)
    {
        v = conj(v);
    }

    fastFourierTransform(x);

    for (complex<float>& v : x)
    {
        v = conj(v);

        v /= x.size();
    }

    vector<float> samples(x.size());

    for (size_t i = 0; i < x.size(); i++)
    {
        samples[i] = x[i].real();
    }

    return samples;
}
```

I'm mostly not going to attempt to explain how this works because I honestly have no idea and you can basically just treat it as a black box in your code, but there are a few things to note. Firstly note that we use complex numbers to represent the frequency buckets inside the Fourier transform. Secondly you should note the assertion at the start of the `fft` function. This implementation of the FFT is only compatible with blocks of data that are exactly a power-of-two in size (e.g. 256, 512, 1024, 2048, etc.). If the function is called with a non-power-of-two sized block it won't crash but the result will be extremely corrupted. Fortunately for us we can tell SDL what size blocks we'd like to handle and we already picked `2048` when we first called `SDL_OpenAudio`.

Once we've done a Fourier transform on our data we can apply the easiest effect to it: a [Doppler shift](https://en.wikipedia.org/wiki/Doppler_effect). For those who are unaware the Doppler shift is the change in the pitch of a sound due to relative motion of the source and listener. The typical example is a car speeding down a road towards you, which will sound higher pitched than it really is as it approaches and then lower pitched as it recedes into the distance. This occurs because as the car approaches each sound wave is emitted from a point closer to you than the previous one, meaning it "catches up" a little bit, shortening the gap between wave peaks and so raising the frequency. The reverse happens as the car speeds away, stretching the sound. There are animations on the Wikipedia page I linked above that demonstrate this effect in a visual way that's easy to understand.

This effect can be a little subtle so we're going to switch back to using a tone rather than music to make it more obvious:

```
audio = generateTone(440, 30);
```

This effect will be most obvious if the sound source speeds past one of our ears so we'll change the sound source's movement to this:

```
sourcePosition = vec3(sourceStartPosition + sourceVelocity * elapsed, 0);
```

We also change the start position to be ten meters behind the right ear and add a new constant to store the velocity vector, which moves forwards at 2 m/s:

```
constexpr vec2 sourceStartPosition = vec2(0.5, -10);
constexpr vec2 sourceVelocity = vec2(0, 2);
```

Having done this we need to alter the behavior of the per-ear effect calculation loop slightly:

```
ears[ear] = mono;

vec3 localEarPosition = localEarPositions[ear];

float distance = localEarPosition.distanceTo(localSourcePosition);
vec3 direction = localEarPosition.directionTo(localSourcePosition);

vec3 relativeVelocity = vec3(sourceVelocity, 0) - listenerVelocity;
float relativeSpeed = direction.dot(relativeVelocity);
float doppler = speedOfSound / (speedOfSound - relativeSpeed);

if (!isinf(doppler) && !isnan(doppler) && doppler > 0)
{
    ears[ear] = dopplerShift(ears[ear], doppler);
}
else
{
    ears[ear] = vector<float>(requestedSamplesPerChannel, 0);
}

float falloff = 1 / pow(1 + distance, 2);
float angle = angleBetween(direction.toVec2().normalize(), rightDir) / 2;
float panning = ear == 0 ? sin(angle) : cos(angle);

for (size_t i = 0; i < requestedSamplesPerChannel; i++)
{
    ears[ear][i] = panning * ears[ear][i] * falloff;
}
```

We need to do our Doppler shift in here because the effect depends on the relative velocity _towards_ or _away from_ the listener, which will actually vary slightly for each ear. Relative velocity means that the listener moving towards the source at 2 m/s and the source moving towards the listener at 2 m/s have the same effect (and if both happen at the same time that's effectively 4 m/s of relative velocity).

We start by settings `ears[ear]` to be immediately equal to the unprocessed sound to make it easier to manipulate the samples. Then we compute the relative velocity by subtracting the listener's velocity (always zero in this example, but I've included it because it won't be zero in a real game) from the source's velocity. Then we compute the relative speed towards or away from the listener by getting the dot product of that with the listener → source direction vector. This value will be positive if the source is moving away and negative if it is approaching. Finally we compute the Doppler multiplier, which tells us how much our sound has been stretched or compressed. If there is no relative velocity this value is 1. As a sound source speeds away from the listener the value climbs, representing a stretching out of the sound waves (and thus a lowering of the observed frequency). As a sound source approaches the listener the value falls, representing a compressing of the sound waves (and thus a raising of the observed frequency).

Important note here, the Doppler multiplier is non-existent (e.g. division by zero) when the sound source is approaching at exactly the speed of sound and nonsensical (e.g. negative) when the sound source is approaching faster than the speed of sound. In practice what this means is you can't hear things that are moving towards you at supersonic speed. This is _roughly_ accurate, if an aircraft is moving towards you at supersonic velocity you won't be able to hear it until the sonic boom hits you, which occurs as the aircraft passes or afterwards depending on its speed. This speed-dependent timing will be slightly wrong with our approach but it's good enough considering the rarity of macro-scale supersonic effects both in real life and games. Open world maps are typically ~8 km across while the speed of sound is ~343 m/s, meaning a truly supersonic aircraft would cross them in 23 seconds or less. Few games contain true supersonic aircraft for this reason. Additionally, when the player is experiencing supersonic effects they're usually _in_ an aircraft which comes with both isolation from the external environment and significant engine noise that obscure ambient sound sources.

To account for the possibility of supersonic effects we check if the Doppler multiplier is a valid value (meaning a valid positive number that isn't infinity) and if it is then we perform our Doppler shift. If Doppler multiplier is instead zero, negative, an infinity or NaN the calculation is skipped and instead we fill our buffer with zeroes, rendering the sound silent.

To actually perform the Doppler shift we use a function with this signature, which should be fairly self-explanatory.

```
vector<float> dopplerShift(const vector<float>& samples, float doppler)
```

This function starts by performing the Fourier transform to convert the sound into the frequency domain:

```
valarray<complex<float>> frequencies = fft(samples);
```

Then we create storage for our output and fill it with the shifted values:

```
size_t bucketCount = frequencies.size();

for (size_t i = 0; i < bucketCount / 2; i++)
{
    float inputFIndex = i * doppler;
    
    size_t beforeIndex = floor(inputFIndex);
    size_t afterIndex = ceil(inputFIndex);

    complex<float> beforeInput = beforeIndex >= 0 && beforeIndex < bucketCount / 2
        ? frequencies[beforeIndex]
        : 0;
    complex<float> afterInput = afterIndex >= 0 && afterIndex < bucketCount / 2
        ? frequencies[afterIndex]
        : 0;

    float t = inputFIndex - floor(inputFIndex);

    shiftedFrequencies[i] = beforeInput * (1 - t) + afterInput * t;
}

for (size_t i = 0; i < bucketCount / 2; i++)
{
    float inputFIndex = i * doppler;
    
    size_t beforeIndex = bucketCount - (floor(inputFIndex) + 1);
    size_t afterIndex = bucketCount - (ceil(inputFIndex) + 1);

    complex<float> beforeInput = beforeIndex >= bucketCount / 2 && beforeIndex < bucketCount
        ? frequencies[beforeIndex]
        : 0;
    complex<float> afterInput = afterIndex >= bucketCount / 2 && afterIndex < bucketCount
        ? frequencies[afterIndex]
        : 0;

    float t = inputFIndex - floor(inputFIndex);

    shiftedFrequencies[bucketCount - (i + 1)] = beforeInput * (1 - t) + afterInput * t;
}
```

This looks scary but if you squint it's actually just linear resampling again. As I mentioned earlier the Fourier transform is mirrored. This is because it has positive frequencies on one side and negative frequencies on the other. Lower frequencies are found on the outer edges of the graph while higher ones lie towards the center. Because of this structure we have to handle the two halves differently, moving both sides in towards the center as an sound source approaches and away from the center as it recedes.

To see how it works let's look at an example. Imagine the sound source is approaching at half the speed of sound, giving it a relative speed of `-171.5`. The Doppler factor is then `343 / (343 - -171.5)` or `0.666...` and the sound's pitch should **rise**. Considering what happens to output sample in the first half #200 we see that `200 * 0.666...` is about `133.333` so we're going to be taking intensity from a lower bucket (#133) and moving it into a higher one (#200), moving towards the center. Meanwhile sample #200 in the second half will be flipped around into sample #1847, which is filled using a value from sample #1913, again moving towards the center.

Once we've identified where in the input we're going to take data from the rest of the procedure is simple, we just take the two buckets surrounding the chosen location (if applicable) and linearly interpolate between them to get an approximation of the true value in cases where our chosen location falls between two buckets. I used linear resampling here because it's easy to implement but you can use another more advanced resampling algorithm if you prefer.

Finally we convert back into the time-domain with the inverse Fourier transform and return the result:

```
return ifft(frequencies);
```

And that's it! If you open the example you should hear the sound fly past your right ear, pitched up as it approaches and down as it recedes. You will notice that the sound sounds extremely choppy. This is because of a limitation of doing filtering on chunks of audio that causes the filter to apply incorrectly to the start and end of the chunk. We'll tackle that problem in the next tutorial part.
