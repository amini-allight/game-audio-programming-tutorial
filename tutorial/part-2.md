Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 2: Psychoacoustics, Stereo, Continuous and Discrete Sampling

Last time we managed to play a simple tone sound and even customize it to some degree. In this part I want to go over how stereo works and some other fundamentals we're going to need in later parts. To start with: psychoacoustics. In the last part we covered how sound exists physically, as an analog signal and as a digital one but we didn't cover how humans perceive it. After all when you hear a sound you don't perceive a series of up and down fluctuations happening hundreds of times a second, you perceive a note. The four main components here are as follows:

**Volume** is how loud the sound is. In terms of musical instruments this is the difference between pressing a piano key and pressing it harder<strong>*</strong>. This is controlled by the amplitude of the wave we write into the LPCM data, the distance between the highest highs and the lowest lows. In the last part we generated a sound with a wave swinging from -32,767 to 32,767 and back, the largest possible range and so the loudest possible sound. If you change the code to this multiplying all the samples by `0.5` will result in a sound that's half as loud:

```
audio[i] = 0.5 * sin(frequency * tau * (i / static_cast<float>(systemFrequency))) * 32'767;
```

**Pitch** is the base frequency of the sound. In terms of musical instruments this is the difference between pressing one piano key and pressing the one next to it<strong>*</strong>. This is controlled by the rate at which the wave we write into the LPCM data oscillates, more cycles in one second means a higher frequency and so a higher pitch. If you change the first argument to `generateTone` in the code you'll be able to control this, `generateTone(880, 5)` will generate a sound that sounds much higher pitched while `generateTone(220, 5)` will generate one that sounds lower pitched.

**Timbre** is the "character" or "color" of a sound. In terms of musical instruments this is the difference between pressing a piano key and playing the same note on a violin. They may be the same volume and the same pitch but there is some fundamental difference between them that allows you to tell which is which. Timbre is by far the most complex of these three but the easiest way to describe it is as being stored in the "shape" of the wave. If you open a piece of music in [Audacity](https://www.audacityteam.org/) you won't see a regular wave pattern but a mess of shapes like this:

![](res/music.png)

Part of that is due to multiple overlapping instruments being played but it's also due to timbre. We can control it in the code in a simple way by changing the type of wave we generate. Here's some code that generates a sawtooth wave instead of a sine wave, changing the character of the sound to be more of a "buzz":

```
static void generateTone(float frequency, float duration)
{
    int totalSamples = duration * systemFrequency;

    audio = vector<int16_t>(totalSamples, 0);

    int16_t x = 0;

    for (int i = 0; i < totalSamples; i++)
    {
        audio[i] = x;
        x += pow(2, 16) / (systemFrequency / frequency);
    }
}
```

Now you should have a better understanding of what exactly we're controlling here. Now let's talk about stereo. Stereo LPCM is stored interleaved, starting with the left channel. That means if our storage layout before looked like this:

```
mono0, mono1, mono2, mono3, mono4, ...
```

It now looks like this:

```
left0, right0, left1, right1, left2, ...
```

I don't want to get into panning and 3D sound until a later tutorial so for now we're just going to make the tone only play in your left ear/speaker. To do that we're going to need to set the number of sound channels to 2 so SDL knows to expect interleaved audio:

```
static constexpr int systemChannels = 2;
```

Then we add some new functions to operate on interleaved stereo streams:

```
template<typename T>
tuple<vector<T>, vector<int16_t>> splitStereo(const vector<T>& interleaved)
{
    vector<T> left(interleaved.size() / 2);
    vector<T> right(interleaved.size() / 2);

    for (size_t i = 0; i < left.size(); i++)
    {
        left[i] = interleaved[i * 2 + 0];
        right[i] = interleaved[i * 2 + 1];
    }

    return { left, right };
}

template<typename T>
vector<T> joinStereo(const vector<T>& left, const vector<T>& right)
{
    vector<T> audio(left.size() * 2);

    for (size_t i = 0; i < left.size(); i++)
    {
        audio[i * 2 + 0] = left[i];
        audio[i * 2 + 1] = right[i];
    }

    return audio;
}
```

I've made these template functions because we won't always be representing our samples as `int16_t`. We're also going to modify `generateTone` to return a block of samples rather than interacting with a global:

```
static vector<int16_t> generateTone(float frequency, float duration)
{
    int totalSamples = duration * systemFrequency;

    vector<int16_t> audio(totalSamples, 0);

    for (int i = 0; i < totalSamples; i++)
    {
        audio[i] = sin(frequency * tau * (i / static_cast<float>(systemFrequency))) * 32'767;
    }

    return audio;
}
```

And then we're going to add a function similar to `generateTone` but for generating silence, like this:

```
static vector<int16_t> generateSilence(float duration)
{
    int totalSamples = duration * systemFrequency;

    return vector<int16_t>(totalSamples, 0);
}
```

Now we put it all together in our main function:

```
audio = joinStereo(generateTone(440, 5), generateSilence(5));
```

If you compile and run this you'll hear the same tone as before, but now only in your left ear! With this we have almost everything we need to start work on some more advanced features, but first we need to talk about that 32,767 thing (and one more thing we'll cover in the next tutorial part). So far we've been representing samples as signed 16-bit integers. We've been doing this because this is what operating system audio subsystems generally expect (and eventually the DACs behind them) but for performing the vast majority of signal processing operations we'll be operating on the samples as 32-bit floating points. We do this because floating points are a much better fit for representing a continuous range of possible values. You saw this when we generated our sine wave, the output of `sin` was a float and then we implicitly converted it to an integer. But there's a problem here. The range of floats we'll be using is symmetric, from -1.0 to +1.0. The range of integers we're using on the other hand is asymmetric, from -32,768 to +32,767. This means we cannot convert between them perfectly. Here are some of the dangers to look out for:

```
int16_t continuousToDiscrete(float x)
{
    return x * 32'768;
}
```

This will convert a value of +1 to +32,768 which will overflow and wrap around to -32'768 creating a nasty pop sound.

```
float discreteToContinous(int16_t x)
{
    return x / 32'767.0f;
}
```

This isn't great either, it'll convert a value of -32,768 to about -1.00003, slightly going outside of our intended floating point range. The solution I've chosen and will be using throughout this tutorial is as follows:


```
int16_t continuousToDiscrete(float x)
{
    return x * 32'767;
}

float discreteToContinous(int16_t x)
{
    return x / 32'768.0f;
}
```

This isn't a perfect solution, every time you run audio through this conversion it will become very slightly quieter, but it's good enough for our purposes. You can find more information about this problem [here](http://blog.bjornroche.com/2009/12/int-float-int-its-jungle-out-there.html), including an overview of various audio systems you might recognize and the conversion functions they've chosen to go with.

That's it for this part. In the next part we're going to be covering WAV files and how to save and load them.
