Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, (this part is an exception and has a second dependency, opusfile) which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. You will need to set up the opusfile dependency yourself. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 11: Data Compression for Audio Files

In this part we're going to have a bit of a detour and talk about how to compress audio data. Back when we were discussing loading WAV files I mentioned that while WAV files are easy and compute-unintensive to load they aren't ideal for storing lots of game audio. This is because they take up a lot of disk space, about 5.5 MiB per minute of uncompressed 48,000 Hz mono audio or 11 MiB per minute of stereo. For large games with multi-hour soundtracks and potentially hundreds of hours of dialogue this adds up quick. This can also be an issue for implementing voice chat systems. In an uncommon (but not unheard of) case a multiplayer server might be expected to receive 1 mono voice stream from each of 32 players and then transmit all 32 back to every player. Without data compression that would come to 98 MB/s upload from the server! Fortunately for us audio is very amenable to data compression.

Before we go any further I want to explain why I call I keep calling it **data compression**, rather than just compression. The reason is that compression is unfortunately an overloaded term in audio circles. People working in both audio design and music tend to use the word "compression" without further clarification to refer to [https://en.wikipedia.org/wiki/Dynamic_range_compression](dynamic range compression), which is completely different from data compression. Dynamic range compression refers to reducing the difference between loud sounds and quiet sounds in audio, while data compression refers to making the audio take up less space on the disk or in memory. Dynamic range compression will be the topic of a future audio tutorial (probably two) but this one is about data compression. With that out of the way let's get back to it.

There are fundamentally two kinds of data compression we can choose between: [lossy data compression](https://en.wikipedia.org/wiki/Lossy_compression) and [lossless data compression](https://en.wikipedia.org/wiki/Lossless_compression). Lossy data compression damages the actual data but achieves a very good compression ratio (the ratio of the uncompressed size to the compressed size). This type of compression is generally used for media file formats like images, video and audio where the overall meaning can be preserved even if some of the data is lost. Think of a meme that's been uploaded many times accumulating JPEG compression artifacts but still being readable. Lossless data compression only performs compression operations that can be perfectly reversed. This leaves the data unchanged but achieves a much weaker compression ratio.  This type of compression is generally used for all other files formats: lossy compression of text documents would randomly insert incorrect characters throughout the text and would be unacceptable for almost all use cases, lossy compression of binary files would make them unreadable to the programs they are intended to be read by, higher quality image formats like PNG or TIFF, etc.

For audio we mostly use lossy compression. Lossy compression algorithms for audio are very good, having almost no effect on the listener's experience of the sound, and they allow us to achieve a compression ratio that will actually make a difference to our game's overall file size. Here are some example values from compressing a 3:30 long song (this example was computed with 2 channels, 16-bit samples in the WAV and a 48,000 Hz sample rate):

- **WAV, uncompressed:** 38.4 MiB
- **FLAC, losslessly compressed:** 27.8 MiB
- **Opus, lossy compressed:** 2.3 MiB

As you can see lossy compression is really what we want here so what's what this tutorial is going to focus on. There are many different standards for compressing audio data, commonly referred to as codecs. There are lots of tradeoffs to be made when selecting which codec to use. Some codecs are patent-encumbered and require payments to the rightsholders in order to legally use while others are royalty-free. Some codecs prioritize rapid encoding and decoding while others prioritize the quality of the final decompressed codec at the cost of speed (in many codecs this tradeoff is adjustable to some extent). Some codecs are just plain old and use less advanced techniques to achieve compression and are beaten on every metric by newer more advanced codecs.

I've selected two codecs for us to focus on: [Opus](https://opus-codec.org/) and [QOA](https://qoaformat.org/) (Quite OK Audio Format). Opus is a modern general-purpose codec developed by the Xiph.Org Foundation for the web and it sees widespread use there (e.g. browser-based voice chat, video streaming). It achieves the same audio quality as the venerable MP3 at less than half the size. QOA is a much more niche codec developed by Dominic Szablewski specifically for use in games. It intends to solve specifically the problems encountered by game audio e.g. achieving small file sizes _without_ incurring large decompression-time compute costs. This is because games often operate in compute-constrained environments like consoles and weaker PCs where every CPU cycle counts and decompression can quickly eat into that budget. You can read more about the design decisions that inspired QOA in [this excellent article](https://phoboslab.org/log/2023/02/qoa-time-domain-audio-compression). Both of these codecs are available as free and open source software and aren't patent-encumbered which makes them ideal for small creators. Unlike WAV these codecs are non-trivial to implement so we'll be using off-the-shelf implementations rather than writing our own file loader like we did with WAV. In the case of QOA the implementation is contained in a single header file, `qoa.h`, which I've included in the example. I slightly modified this file, adding explicit typecasts to allow it to compile as C++, but otherwise it's identical to the one found in the QOA repository at the time of writing. For Opus the implementation takes the form of an external library, [opusfile](https://github.com/xiph/opusfile). On Linux, MacOS, BSD, etc. you should be able to acquire this from your system package manager (or [Homebrew](https://brew.sh/) in MacOS' case) but unfortunately it wasn't available in the [Scoop](https://scoop.sh/) package repositories so on Windows you'll have to source it yourself.

To start with let's look at Opus. You can create Opus files by exporting directly to the format from wherever you're mastering your audio (e.g. [Audacity](https://audacityteam.org/), some DAW) or you can output to WAV and then compress using an external utility like [FFmpeg](https://ffmpeg.org/). Opus has the unusual quality that it only supports a 48,000 Hz sample rate: the internal encoding uses 48,000 Hz and the decoded audio will always be 48,000 Hz. A handful of sample rates that are exact divide cleanly into that number (8000, 12000, 16000 and 24000) are supported as input to the encoder in place of 48,000 Hz but they will be converted to 48,000 Hz internally. Once you have your Opus file (which has the extension `.opus`) you can open it from your application. To do this we'll need the opusfile library, which is accessed by including the relevant header:

```
#include <opus/opusfile.h>
```

Then we create a type to hold the recovered data, again there's no need for a `sampleRate` field (unlike with WAV files) because the decoded sample rate will always be 48,000 Hz:

```
struct OpusContents
{
    int channels;
    vector<int16_t> samples;
};
```

And a signature for our new function:

```
inline OpusContents loadOpus(const string& path)
```

Now we can start that function by opening the file:

```
int result = 0;

OggOpusFile* file = op_open_file(path.c_str(), &result);

assert(result == 0);
```

This function is fairly straightforward: it takes two arguments, the path to the file and an integer pointer where the result code will be written and returns an opaque file handle which other functions from the same API will operate on. We check the result is the success value, which is 0, before moving on. It's worth mentioning there are alternatives to this function like `op_open_memory` for reading a file that has already been loaded into your program's memory by some other means (e.g. it was extracted from a ZIP archive or received over the network).

Next we check the file's channel count is as required, you might want to change this if you want to load stereo audio (e.g. for music, pre-rendered cutscene audio or unspatialized dialogue like audio diaries).

```
int channels = op_channel_count(file, 0);

assert(channels == 1);
```

The second argument to this function is the "link index". This is related to multi-part files commonly used for streaming and can be ignored for our purposes. Now we get the number of samples in the file and create a buffer containing that many samples to store our audio in:

```
int sampleCount = op_pcm_total(file, 0);

assert(sampleCount > 0);

vector<int16_t> samples(sampleCount, 0);
```

The second argument here is again the link index. This function can fail and the errors are communicated as negative error codes so we can check for both error codes and empty audio files with the same assertion. Now we just need to load the audio. This is done using two different functions: one for stereo and another for mono:

```
if (channels == 2)
{
    result = op_read_stereo(file, samples.data(), samples.size());
}
else
{
    result = op_read(file, samples.data(), samples.size(), nullptr);
}

assert(result == sampleCount / channels);
```

The third argument to `op_read` is an optional output argument for the last link index read from by the operation. Finally we clean up by closing the file to make sure we don't leak any memory and then exit the function:

```
op_free(file);

return {
    channels,
    samples
};
```

Now let's look at QOA. QOA is a very new and niche protocol so you probably aren't going to be able to export directly to it from your audio software. FFmpeg does support it but unfortunately for decoding only. Instead you'll have to use the `qoaconv` command-line utility which is acquired by compiling the [QOA repository](https://github.com/phoboslab/qoa). Export WAV files from wherever you do your audio mastering and then run `qoaconv in.wav out.qoa` to convert them. Once you have your QOA file we can load it and play the audio within. To do this first we include the header:

```
#define QOA_IMPLEMENTATION
#include "qoa.h"
```

The header contains both normal header content (e.g. type definitions and function declarations) and a full implementation of the QOA codec. The second part is wrapped in `#ifdef QOA_IMPLEMENTATION` tags in case you want to include it into multiple source files without compiling multiple copies of the implementation, which would violate the [One Definition Rule](https://en.wikipedia.org/wiki/One_Definition_Rule). We only have one source file, `example.cpp`, so we'll just enable the implementation here with `#define QOA_IMPLEMENTATION`.

Next we define a type to hold the extracted values and the signature of our function, this is stuff we've seen before:

```
struct QOAContents
{
    int channels;
    int frequency;
    vector<int16_t> samples;
};

inline QOAContents loadQOA(const string& path)
```

Now comes the implementation of loading function, starting with a call to `qoa_read`:

```
qoa_desc description;

auto rawSamples = static_cast<int16_t*>(qoa_read(path.c_str(), &description));

assert(rawSamples);
assert(description.channels == 1);
assert(description.samplerate == systemFrequency);
assert(description.samples > 0);
```

This function takes the path to the target file and returns two values: a `qoa_desc` object by way of the second argument and an array of samples by way of the actual return value. The return value is `void*` for some reason so we'll need to cast it, then do our checks to make sure everything is how we expect. Next we can get the samples in an easier-to-use form:

```
vector<int16_t> samples(description.samples * description.channels, 0);

memcpy(samples.data(), rawSamples, samples.size() * sizeof(int16_t));

free(rawSamples);
```

I'm using `free` to release the returned memory here because it was allocated with `malloc`. It's possible to change this behavior if you want to use a custom allocator by defining the macros `QOA_MALLOC` and `QOA_FREE` before including `qoa.h`. Finally we can just return the recovered data:

```
return {
    static_cast<int>(description.channels),
    static_cast<int>(description.samplerate),
    samples
};
```

And that's it! You should now be able to use either `loadOpus("../../res/music-mono.opus")` or `loadQOA("../../res/music-mono.qoa")` to load the music instead of loading the WAV.

Which one of these you end up using (or even what mixture) is going to depend on your specific requirements. For a game with a relatively fixed pool of sounds (e.g. a game with levels, each with a fixed pool of sound effects and scripted dialogue) you should probably use Opus: decompress all the necessary sounds into memory upon level load and rely on the fact that the vast majority of PCs on Steam have 16+ GB of RAM and once the successor to the Nintendo Switch ships all current consoles will have 8+ GB of RAM. For a game with a much more open-ended sound pool (e.g. an open-world game with hundreds of hours of dialogue) you'll need to load and decompress sounds from the disk as they are become necessary due to the player's actions. For this you'll probably want to use QOA: decompress a pool of critical sound effects (e.g. physics and combat sounds) when the player enters the game and then rely on dynamic loading and decompression for anything situational. In either case it's important to benchmark the disk space, quality and decompression speed so you can make an informed decision about the tradeoffs involved. In the next part we'll take a look at how to implement audio encoding and decoding for voice chat.
