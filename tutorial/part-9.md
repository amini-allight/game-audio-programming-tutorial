Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 9: Processing Overlapped Chunks

In the last tutorial we implemented a doppler shift using Fourier transforms but we had a bit of a problem. The problem was that Fourier transforms (and most of the other techniques we're going to be using later in this series) operate on frequencies, but frequencies are only obvious across spans of time. If I show you a single sample the value might 200, -23,832, 9318 or any other signed 16-bit integer, but that tells you nothing about the frequency being represented. To see the frequency you need to see a number of other samples surrounding the sample so you can see the line they draw as they sway up and down. This causes issues when processing the frequencies in chunks of realtime audio, as we do in a game's audio subsystem. It becomes impossible to accurately determine the frequencies towards the start and end of each chunk. These frequencies are then incorrectly processed. If this only happened once it might be okay but it happens at the start and end of every single chunk, dozens of times a second. The result is a "choppy" sound where the chunks are very evident due to the difference between the middle of each chunk (which is processed correctly) and the ends (which are processed incorrectly).

The easiest and most thorough way to fix this is just to overlap the chunks. We do all our filtering on chunks of say, 1024 samples, and then we only submit the middle 512 samples to the operating system, discarding the 256 samples at the two ends. We then advance the read head by only the width of the middle part, ensuring we don't actually skip any audio. This can be a bit confusing so here's a visualization:

![](res/chunks.png)

You can think of this as reading some of the previous chunk and some of the next chunk along with the chunk we're actually interested in. In the case of the first chunk read for a given sound this will mean reading before the start (e.g. if the real chunk starts at index 0 the prefix overlap will run from indices -256 to -1 inclusive) but we can just pad this with zeroes and it should work fine. Likewise we'll need to read past the end of the sound when processing the final chunk, but again we can just pad with zeroes. We've actually been doing this at the end of the sound since the very first tutorial to handle situations where the length of our sound wasn't a perfect multiple of the length of our chunk.

Now let's look at what this looks like in practice, starting with our constants. First up is a new constant, `overlappedBufferSize`. This is the size of our "working buffer", the buffer we'll actually apply filters and effects to, so it needs to be a power-of-two or the Fourier transforms will fail.

```
static constexpr int overlappedBufferSize = 1024;
```

I've reduced the value from the old working buffer size (2048) to 1024 because I actually found that with such large chunks you could hear seams between chunks just from how much the position and velocity information changed between one chunk and the next. Next we have the overlap size, this is how much of our chunk is used for overlapping:

```
static constexpr int overlapSize = 256;
```

This is a very large overlap, possibly unnecessarily large, but SDL recommends the output buffer also be a power-of-two size which this combination of working buffer size and overlap size provides. This is also the value I use in LMOD, so I know it works in a wide variety of situations. The tradeoff here is between wasted compute (the larger the overlap is the more time you spend calculating effects for the same samples over and over, only to discard them) and ability to accurately detect the frequencies present in the signal, particularly low frequencies that are only visible over longer spans of time. You can try dialing in the value in your game to hit the best balance here.

We still have our `systemBufferSize` constant which we give to SDL but now it describes the size of the middle section of the buffer with the overlaps removed, not the entire working buffer. As a result it's defined like this:

```
static constexpr int systemBufferSize = overlappedBufferSize - overlapSize * 2; // samples per channel
```

With those done we can start modifying our application's behavior. We start by adding a new tool function, which just joins two vectors together:

```
template<typename T>
vector<T> join(const vector<T>& head, const vector<T>& tail)
{
    vector<T> joined;

    joined.reserve(head.size() + tail.size());
    joined.insert(joined.end(), head.begin(), head.end());
    joined.insert(joined.end(), tail.begin(), tail.end());

    return joined;
}
```

Then we use it to append an overlap-sized block of silence to the start of our source audio, which will serve as the first chunk's prefix overlap:

```
audio = join(vector<int16_t>(overlapSize, 0), generateTone(440, 30));
```

Now we need to modify our `onOutput` function to load and process the sound differently. The `monoDiscrete` buffer will now have the working buffer's size, not the general output size:

```
vector<int16_t> monoDiscrete(overlappedBufferSize, 0);
```

Then we change the requested size to the size (in bytes) of the same buffer:

```
int requestedSize = overlappedBufferSize * sizeof(int16_t);
```

Then when advancing the read head we move by the width (in bytes) of the middle part _without_ overlaps.

```
readHead += systemBufferSize * sizeof(int16_t);
```

Then we replace all remaining instances of `requestedSamplesPerChannel` with `overlapBufferSize` because that's now the size of our working buffer:

```
if (!isinf(doppler) && !isnan(doppler) && doppler > 0)
{
    ...
}
else
{
    ears[ear] = vector<float>(overlappedBufferSize, 0);
}
```

```
for (size_t i = 0; i < overlappedBufferSize; i++)
{
    ...
}
```

And then finally we just need to cut the overlaps off before submitting the buffer, for which we'll need another helper function:

```
template<typename T>
vector<T> subBuffer(const vector<T>& buffer, size_t start, size_t count)
{
    return {
        buffer.begin() + start,
        buffer.begin() + (start + count)
    };
}
```

This behaves the same as `std::string::substr`, returning a new vector that contains the elements from the original vector starting at `start` and continuing for `count` items. Then we just have to use it to extract the center of each working buffer:

```
vector<float> stereo = joinStereo(
    subBuffer(ears[0], overlapSize, systemBufferSize),
    subBuffer(ears[1], overlapSize, systemBufferSize)
);
```

And that's it! If you listen to the sound now you should hear it fly past your right ear without any of that distortion it contained in the last tutorial part.
