Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 14: Capturing the User's Microphone

Over the last two tutorials we've looked at how to compress audio data for network transmission for use in voice chat however until now we've been practicing on a pre-recorded sample of audio. In this part I just want to briefly go over what capturing audio from the user's microphone looks like. There isn't terribly much complexity here but I thought it was important to go over it for completeness sake. We'll access the microphone via SDL, the same as we did the speakers, so large parts of this tutorial will be specific to that API. Most other APIs should be similar with the only major difference being that some of them (OpenAL) use a pull model where you must poll the API periodically to retrieve the captured audio rather than a push model where it's provided to you via a callback.

To start with we need to open the audio input device. This is very similar to how we originally opened the audio output device all the way back in part 1.

```
SDL_AudioSpec inputDesired{};
inputDesired.freq = systemFrequency;
inputDesired.format = AUDIO_S16SYS;
inputDesired.channels = 1;
inputDesired.samples = systemBufferSize;
inputDesired.callback = onInput;

SDL_AudioSpec inputObtained{};

int inputDevice = SDL_OpenAudioDevice(
    nullptr,
    true,
    &inputDesired,
    &inputObtained,
    SDL_AUDIO_ALLOW_SAMPLES_CHANGE
);

if (!inputDevice)
{
    cerr << "Failed to open audio input device: " << SDL_GetError() << endl;
}
```

As before we specify that we want to receive the audio at our desired sampling rate (48,000 Hz) and in signed 16-bit samples. We're only going to ask for a single channel because most microphones aren't stereo and we might want to spatialize our audio for proximity voice chat, which requites it to start out as mono. Finally we're going to pass in a callback, `onInput`, which SDL will call every time it has a buffer of audio for us to process. We'll ask for the buffer to be our `systemBufferSize`. Then we call `SDL_OpenAudioDevice` to open the device. The arguments to this are an optional string name for the device (e.g. if you show the user a dropdown and get them to pick their preferred audio device by name), `iscapture` which is true in this case because this will be an input device, our requested audio configuration, another structure for SDL to return the _actual_ audio configuration and finally a set of flags. These flags tell SDL which parts of our audio configuration it's allowed to compromise on. We won't be doing any processing on our audio input chunks directly (all valid Opus chunk sizes are non-power-of-two while SDL strongly recommends a power-of-two size so even if we were using compression here we'd still want a delay buffer in between) so we tell SDL it's okay to compromise on the buffer size. If the function succeeds it'll return a positive integer that uniquely identifies the device, otherwise it will return 0.

With that out of the way we just unpause the input device alongside our output device and enter the usual sleep:

```
SDL_PauseAudioDevice(outputDevice, false);
SDL_PauseAudioDevice(inputDevice, false);

this_thread::sleep_for(chrono::seconds(30));
```

After the sleep we just need to clean up the new device with one function call:

```
SDL_CloseAudioDevice(inputDevice);
SDL_CloseAudioDevice(outputDevice);
```

While the main loop is sleeping all the interesting things will happen in our callbacks. Let's look at the input one first:

```
static void onInput(void* userdata, uint8_t* input, int inputSize)
{
    vector<int16_t> samples(inputSize / sizeof(int16_t), 0);
    memcpy(samples.data(), input, inputSize);

    audio = join(audio, samples);
}
```

This is pretty simple, it just receives audio bytes from SDL and adds them to the back of the audio buffer. Now we have our output callback, which I've simplified to focus in on the relevant parts:

```
static void onOutput(void* userdata, uint8_t* output, int outputSize)
{
    assert(outputSize == systemBufferSize * systemChannels * sizeof(int16_t));

    vector<int16_t> monoDiscrete(systemBufferSize, 0);

    if (audio.size() >= systemBufferSize)
    {
        // All sizes are in bytes
        auto input = reinterpret_cast<const uint8_t*>(audio.data());
        int inputSize = audio.size() * sizeof(int16_t);
        int remainingSize = inputSize;
        int requestedSize = systemBufferSize * sizeof(int16_t);
        int readSize = min(requestedSize, remainingSize);
        memcpy(monoDiscrete.data(), input, readSize);
        audio.erase(audio.begin(), audio.begin() + systemBufferSize);
    }

    vector<int16_t> stereoDiscrete = joinStereo(monoDiscrete, monoDiscrete);

    memcpy(output, stereoDiscrete.data(), stereoDiscrete.size() * sizeof(int16_t));
}
```

Here we create a buffer of silence of the requested size. If the audio buffer isn't yet big enough (e.g. because the output callback has been called for the first time before the input one) we just mirror this into stereo and submit it. Otherwise we grab a `systemBufferSize` chunk of audio from the `audio` buffer and then cut that off the front of the buffer. By doing this we've constructed a realtime stream between our microphone input and our speaker output. If you run the example with a microphone connected and unmuted you should be able to hear yourself through your own speakers/headphones. Just be aware if you use speakers that you might get a feedback loop where the microphone picks up the audio from the speakers and causes the speakers to repeat it a second time, but louder. This can create a shrill sound that rapidly rises in volume and sounds very unpleasant, so be careful. If this happens it isn't a problem with the code it's just an unfortunate but expected behavior of audio systems. It _is_ possible for the application to use signal processing to attempt to detect its own output returning via the input and suppress it, some voice chat applications do this, but most games don't and it's outside of the scope of this tutorial. Next time we'll return to looking at audio processing techniques.
