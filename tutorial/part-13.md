Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, (this part is an exception and has a second dependency, opus) which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. You will need to set up the opus dependency yourself. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 13: Handling Unreliable Transmission in Voice Chat

In the last part we implemented compression for voice chat, but we did so with the assumption that the voice stream would be transmitted via a reliable network link that prevents packet loss. However this is probably not the most common use case and .

First let's do a quick overview of the network behaviors in play here. In a networked multiplayer game we have multiple copies of the game running on different computers communicating with each other over the local network or the internet via some network protocol. Network protocols are usually stacked, with each network protocol adding on functionality to the one underneath. An example would be a web request. The web-specific parts are encoded in the HTTP protocol which is packaged inside of the TLS protocol for encryption and security. Underneath that the TCP protocol is used to ensure the request arrives without corruption and underneath that the IP protocol is used to deliver the request. In the case of our game it will usually be using a game-specific protocol which is almost always built on top of one of two transport protocols: [TCP](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) or [UDP](https://en.wikipedia.org/wiki/User_Datagram_Protocol).

TCP is reliable. It implements verification, acknowledgement and retransmission logic, invisible to our game, which make sure that if the message arrives at all it arrives complete and in-order. This is great and is the reason most things (e.g. the web) use TCP as their backbone but it can make the delivery of packets slower than it otherwise would be which is bad for games where every millisecond of latency matters. However this hasn't stopped many popular games from making it work anyway.

UDP is unreliable. The application fires packets in the general direction of the remote computer and that's it. They may arrive out of order and some may never arrive at all but at least they will arrive quickly. This is fairly common for games, especially those with realtime gameplay that benefit the most from the lowest latency. Often a game protocol will re-implement parts of TCP's functionality on top of UDP, ensuring reliable transmission for some data (e.g. vital state deltas) while allowing other less-critical data (e.g. user inputs) to possibly suffer packet loss.

If your game is using UDP you're probably going to be treating voice chat packets as less-critical data that can afford to suffer packet loss and if you are [Opus](https://opus-codec.org/) has some functionality that can help with that.

Opus' solution to this is called [Forward Error Correction (FEC)](https://en.wikipedia.org/wiki/Error_correction_code). It encodes extra redundant data in each packet which allows the decoder to estimate the contents of the previous packet. What this means is we can receive packet #1, lose packet #2 and then reconstruct an approximation of packet #2 when packet #3 arrives, using the redundant data contained within. This will of course increase the bandwidth usage slightly (in my testing there's less than 1 extra byte per packet) but in exchange we get significant resistance to packet loss.

To enable this feature we need to modify the initialization of our encoder, which ends up looking like this:

```
VoiceEncoder(int channels, int frequency, int bitrate, float packetLoss)
    : channels(channels)
{
    int status;

    encoder = opus_encoder_create(frequency, channels, OPUS_APPLICATION_VOIP, &status);
    assert(encoder);
    assert(status == OPUS_OK);

    status = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(bitrate));
    assert(status == OPUS_OK);

    status = opus_encoder_ctl(encoder, OPUS_SET_PACKET_LOSS_PERC(packetLoss * 100));
    assert(status == OPUS_OK);

    status = opus_encoder_ctl(encoder, OPUS_SET_INBAND_FEC(packetLoss != 0));
    assert(status == OPUS_OK);
}
```

Compared with the version from before we have a new argument, `packetLoss`, which encodes what fraction of packets we expect to be lost between 0 and 1. Based on this value we initialize two more settings of the encoder. The first is the packet loss percentage, which tells the encoder how heavy we expect the packet loss to be. The second is the in-band FEC setting which we enable when packet loss is expected. Aside from these changes everything about the encoder is the same so let's move on to the decoder.

The decoder is again mostly the same as before, with one major change to the decode function itself.

```
vector<int16_t> decode(const vector<uint8_t>& encoded, bool recoverPacket)
{
    int samplesPerChannel = opus_decoder_get_nb_samples(decoder, encoded.data(), encoded.size());

    assert(samplesPerChannel >= 0);

    vector<int16_t> samples(samplesPerChannel * channels, 0);

    int status = opus_decode(
        decoder,
        encoded.data(),
        encoded.size(),
        samples.data(),
        samples.size() / channels,
        recoverPacket
    );

    assert(status >= 0);

    return { samples.begin(), samples.begin() + status * channels };
}
```

The function has an extra argument: `recoverPacket`. When set to false the result of the function will be the data from this packet, as usual. When set to true however the function will return an approximation of the data in the _previous_ packet, which was lost. This is how we will manage to partially recover from the loss of single packets. Now that we've modified the encoder and decoder we can use them in a little toy example to demonstrate how you might recover from packet loss. This example is obviously abstracted, I'm not going to put a whole network implementation in here, but it works just the same.

Once again we initialize our encoder and decoder:

```
constexpr int voiceChannels = 1;
constexpr int chunkSize = 2880 * voiceChannels;
constexpr int bitrate = 12000;
constexpr float packetLoss = 0.25;
constexpr bool useFEC = true;

VoiceEncoder encoder(voiceChannels, systemFrequency, bitrate, packetLoss);
VoiceDecoder decoder(voiceChannels, systemFrequency);
```

New here are the packet loss fraction which is used to initialize the encoder and a constant called `useFEC`. This is a simple flag we can use to toggle the error correction behavior on and off. In a real application you probably always want this to be on but here it's useful so you can easily switch back and forth and hear the difference error correction makes. Next we create fake packets, as though we were going to send them over a network connection:

```
vector<VoicePacket> packets;

for (int64_t i = 0; i < ceil(contents.samples.size() / static_cast<float>(chunkSize)); i++)
{
    vector<int16_t> chunk(chunkSize, 0);

    memcpy(
        chunk.data(),
        contents.samples.data() + chunkSize * i,
        min<size_t>(chunkSize, contents.samples.size() - chunkSize * i) * sizeof(int16_t)
    );

    vector<uint8_t> encoded = encoder.encode(chunk);

    packets.push_back({ i, encoded });
}
```

This is the same as our previous example. We take an Opus-compatible chunk of audio from the input, possibly padded with silence if the input is not an exact multiple of the chunk size and encode it. Except now we store each encoded chunk separately in a `VoicePacket` structure, which is defined like this:

```
struct VoicePacket
{
    int64_t index;
    vector<uint8_t> encoded;
};
```

This first part is a simulation of what the sender in our game (e.g. a player who has their mic open) would do. Next we simulate packet loss, which happens in the wire between the sender and receiver:

```
random_device device;
mt19937 engine(device());
uniform_real_distribution<float> distribution(0, 1);

for (size_t i = 0; i < packets.size();)
{
    if (distribution(engine) < packetLoss)
    {
        packets.erase(packets.begin() + i);
    }
    else
    {
        i++;
    }
}
```

We use the random functions from the C++ standard library to pick packets at random using our packet loss percentage from earlier. Note I've picked an extremely high packet loss percentage, 25%, to make the effect as obvious as possible. In a real application this will probably be on the extreme end of possible scenarios and personally if I had packet loss ≥ 25% for more than a few minutes I would probably quit the game and go do something else.

Finally we simulate what the receiver (e.g. another player receiving the voice stream) will do:

```
int64_t lastPacketIndex = -1;

for (const VoicePacket& packet : packets)
{
    vector<int16_t> decoded;

    int64_t gap = packet.index - (lastPacketIndex + 1);

    if (gap == 0)
    {
        decoded = decoder.decode(packet.encoded, false);
    }
    else
    {
        decoded = vector<int16_t>((gap - 1) * chunkSize, 0);
        decoded = join(decoded, useFEC ? decoder.decode(packet.encoded, true) : vector<int16_t>(chunkSize, 0));
        decoded = join(decoded, decoder.decode(packet.encoded, false));
    }

    lastPacketIndex = packet.index;

    audio = join(audio, decoded);
}
```

Here we keep track of the index of the last packet we encountered. If the `gap` between the current packet index and the previous packet index is zero then no packet loss has occurred and we can decode like normal, setting the `recoverPacket` argument to false. If the gap is non-zero however packet loss has occurred and we need to attempt recovery. There is the possibility here that multiple packets will have been lost, in which case we have no choice but to fill the gap with silence. To account for this we start by initializing the `decoded` buffer to contain `gap - 1` chunks of silence. If the gap equals 1 this will be of zero length, indicating full recovery. Next we recover an approximation of the most recently lost packet using error correction and append that to the `decoded` buffer. Note that we've set the `recoverPacket` argument to `VoiceDecoder::decode` to true to recover this packet rather than the normal one. Finally we call the decoder a second time to get the most recent normal packet. To complete the loop we update the last packet index variable and add the recovered audio to the overall buffer. Note that this implementation **assumes all packets are the same size**. If your implementation allows senders to send packets of different sizes you will need more complex logic here to estimate the length of lost audio when multiple consecutive packets are lost.

If you run the program with `useFEC` set to true and false you should hear the difference. Both are pretty low-quality audio, 25% of the stream is being lost after all, but the one with FEC enabled is significantly more intelligible. Here's a picture of what it looks like zoomed in on the waveform in Audacity:

![](res/fec.png)

In the next part we'll finish up our journey into voice chat by looking at how we capture audio from the user's microphone.
