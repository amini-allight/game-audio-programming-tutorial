Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 5: Interaural Intensity Difference and Listener Rotation

The next easiest localization effect we can implement is the **Interaural Intensity Difference (IID)**. As a quick refresher, this is the simple fact that, for all sounds that don't originate on the centerline of the listener's head, one ear is slightly closer to the sound source than the other. This means the more distant ear will hear a very slightly quieter version of the sound than the closer one, for reasons of pure distance alone before any shadowing effects from the listener's head obstructing the sound waves come into play.

This effect is very easy to implement and honestly we could have done it in the last part but I wanted to separate it out to drive home an important point: from here on out we're going to be treating each ear as a completely separate sound receiver with its own spatial transform. This also gives me an opportunity to upgrade from the rather basic 3D math setup we had in the previous part to a more robust one that will be more suitable for future parts.

To start with, instead of describing the listener as solely a position we instead use a 4×4 transformation matrix, which allows us to incorporate rotation as well. This lets sounds change when the player turns around or looks up and down (or tilts their head, if they're playing in VR, a flight simulator or some other mode where three-axis rotation of the player character's head is possible). It's important that we don't apply any "scale" components to this matrix as this will distort our sounds' apparent distances in strange and unexpected ways.

```
constexpr mat4 listenerTransform = mat4();
```

Second we add a new constant detailing where the ears are relative to the center of the listener:

```
constexpr vec3 localEarPositions[2]{
    vec3(-0.075, 0, 0),
    vec3(+0.075, 0, 0)
};
```

The left ear is 7.5 cm to the left while the right ear is 7.5 cm to the right, which is pretty much the human average. Now we replace our sound processing code in `onOutput` from the previous part with a new version. First we find the position of the sound in local space:

```
vec3 localSourcePosition = listenerTransform.toLocal(sourcePosition);
```

This can be a bit confusing if you aren't experienced with linear algebra but I'll try explain as best I can. Essentially what we're doing here is finding the position of the sound _from the perspective of the head_. Imagine a sound is coming from the roof in a room, but the player character is lying on the floor on their right side. This means their left ear is pointing at the sound. This operation then basically does this: `lying on your right side + sound above you = sound to your left, relatively speaking`. Next we allocate some space to store our two channels, this time in an array so we can easily iterate over them:

```
vector<float> ears[systemChannels]{ vector<float>(requestedSamplesPerChannel) };
```

```
for (size_t ear = 0; ear < systemChannels; ear++)
{
    vec3 localEarPosition = localEarPositions[ear];

    float distance = localEarPosition.distanceTo(localSourcePosition);
    vec3 direction = localEarPosition.directionTo(localSourcePosition);

    float falloff = 1 / pow(1 + distance, 2);
    float angle = angleBetween(direction.toVec2().normalize(), rightDir) / 2;
    float panning = ear == 0 ? sin(angle) : cos(angle);

    for (size_t i = 0; i < requestedSamplesPerChannel; i++)
    {
        ears[ear][i] = panning * mono[i] * falloff;
    }
}
```

Then we iterate through our two ears. We use a different position for each, calculate different `distance `and `direction` values and so compute slightly different values for `falloff` and `angle` for each ear. Finally we iterate over all the samples and apply these values to the ear we're currently working on. Now there will be a slight intensity difference between the two ears (from spherical distance falloff, not just stereo panning) that wasn't there before. And our code can now support a rotated listener, if you change the listener position to this:

```
constexpr mat4 listenerTransform = mat4(vec3(), quaternion(vec3(0, 1, 0), radians(180)));
```

This flips the listener upside down. If you open the client with this code you'll now hear the sound appears to circle your head in an anti-clockwise direction.
