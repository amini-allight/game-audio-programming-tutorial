Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 6: Resampling

So far we've been doing everything in a common sampling frequency: 48,000 Hz. Our system outputs audio to the operating system at this sampling frequency, expects the files loaded from the disk to use this frequency and does all internal sound math in this frequency as well. This is generally good practice, but what do we do if we want to play a sound file that has a different sampling frequency? If we know that we're going to be playing this specific file (as is the case with most sounds used in a game) we can convert the file to a different sampling frequency ahead of time, for example using [FFmpeg](https://ffmpeg.org/):

```
ffmpeg -i my-44100hz-sound.wav -ar 48000 my-48000hz-sound.wav
```

This is called _resampling_. It's a necessarily imperfect process, and lossy when reducing the sample rate, but works good enough in practice. But what if we don't know what sound we're going to be playing? What if it's retrieved from the network or supplied by the user? In this case we need to do resampling inside of our application at runtime. This might sound like a pretty niche use case but once we have the ability to resample audio at runtime we can use it for another important task: changing the playback speed of a sound. Imagine you have one second of audio at 48,000 Hz: obviously you'll have 48,000 samples. Now imagine you resample this audio to have a sampling frequency of 96,000 Hz: the same sound is now expressed in 96,000 samples. Finally imagine you play the 96,000 Hz audio back at 48,000 Hz (e.g. giving 48,000 samples to the OS every second). The sound will now take two seconds to play rather than one, and it will sound slowed down as a result. This will naturally reduce the sound's pitch as any frequencies found within it will now take twice as long to complete a full cycle.

To demonstrate this I've included a file called `music-mono-24000.wav`, which is our usual demo music but resampled to 24,000 Hz. You can load this sound in place of the normal one:

```
WAVContents contents = loadWAV("../../res/music-mono-24000.wav");
```

Note that you will have to disable the assertion that checks a loaded WAV's sampling frequency matches the system frequency, otherwise it will kill the example program:

```
// assert(formatHeader.sampleRate == systemFrequency);
```

If you load this sound you'll hear it sounds sped up and higher pitched. Our goal in this tutorial will be to fix this sound so it sounds normal again. To start with we're going to do the obvious thing and just [linearly interpolate](https://en.wikipedia.org/wiki/Linear_interpolation) between the samples found in the input. Regardless of whether we're increasing or decreasing the sample rate every sample in the output will lie between (or on top of) two samples in the input. The function to do this looks like this:

```
inline vector<float> linearResample(const vector<float>& input, float scale)
{
    vector<float> output(ceil(input.size() * scale), 0);

    for (size_t i = 0; i < output.size(); i++)
    {
        float elapsed = i / static_cast<float>(output.size());

        size_t aIndex = elapsed * input.size();
        size_t bIndex = aIndex + 1;

        if (bIndex >= input.size())
        {
            output[i] = input[aIndex];
        }
        else
        {
            float t = (elapsed - aIndex / static_cast<float>(input.size())) / (1.0f / input.size());

            output[i] = input[aIndex] * (1 - t) + input[bIndex] * t;
        }
    }

    return output;
}
```

Our function takes an array of input expressed as continuous samples and returns an array of output expressed as continous samples. It also takes an argument called `scale` which is amount to squeeze or stretch the sound. For example, a `scale` of 2 will double the number of output samples: stretching the sound and making it sound lower and slower. A scale of 0.5 meanwhile will halve the number of output samples: squeezing the sound and making it sound higher and faster. A `scale` of 1 will leave the sound unchanged, aside from the potential effects of floating point imprecision.

In terms of how it works this function simply finds the two input samples surrounding the output sample and draws a straight line between them, taking the value at the point on that line that corresponds to the output sample's timing. There's a special case for the final output sample, this sample isn't followed by another sample so we can't interpolate, instead we assume it's the same as the final sample of the input.

This function is a bit of a mixed bag. On the one hand it's very fast, only needing to consider two input samples per output sample, but on the other hand it's quite crude. On some sounds it will sound fine but on others it will act as a bad low pass filter (meaning it destroys high frequencies while retaining low ones), eliminating subtle high frequencies in the sound and introducing artifacting.

The next function we're going to look at is [hermite interpolation](https://en.wikipedia.org/wiki/Hermite_interpolation). This approach considers four input samples for each output sample. The function to do this looks like this:

```
inline vector<float> hermiteResample(const vector<float>& input, float scale)
{
    vector<float> output(ceil(input.size() * scale), 0);

    for (size_t i = 0; i < output.size(); i++)
    {
        float elapsed = i / static_cast<float>(output.size());

        ssize_t bIndex = elapsed * input.size();
        ssize_t aIndex = bIndex - 1;
        ssize_t cIndex = bIndex + 1;
        ssize_t dIndex = bIndex + 2;

        float a = aIndex >= 0 && aIndex < input.size() ? input[aIndex] : input[bIndex];
        float b = input[bIndex];
        float c = cIndex >= 0 && cIndex < input.size() ? input[cIndex] : input[bIndex];
        float d = dIndex >= 0 && dIndex < input.size() ? input[dIndex] : input[bIndex];

        float t = (elapsed * input.size()) - bIndex;

        float c0 = b;
        float c1 = (c - a) * 0.5f;
        float c2 = a - (b * 2.5f) + (c * 2.0f) - (d * 0.5f);
        float c3 = ((b - c) * 1.5f) + ((d - a) * 0.5f);
        output[i] = (((((c3 * t) + c2) * t) + c1) * t) + c0;
    }

    return output;
}
```

This function has the same basic structure as before: take in an array of continuous samples, adjust its length according to the `scale` value, return an array of continuous samples. The main thing that's changed is how the final value is taken, with much more complex weights being used to mix the four input values together. Also changed is the handling of the start and end of the sample array, because this takes more sames as input there's more potential for the necessary samples to be unavailable, in which case we have to use a placeholder value like zero. Using zero is fine when processing whole sounds by themselves, it's valid to think of the sound as preceded and followed by silence, but issues arise when processing an audio stream this way. When resampling an audio stream it's necessary to resample one chunk at a time but what precedes and follows each chunk _isn't_ silence, it's the previous chunk and the next one. We'll be revisiting this problem in a later tutorial part but just be aware it exists for now.

Overall this function is the middle of the road, taking more time than more complex functions but producing a much better result than linear interpolation, good enough for any purpose I've tested.

There are other interpolation functions which are more complex than hermite while producing a better result such as [sinc interpolation](https://en.wikipedia.org/wiki/Whittaker%E2%80%93Shannon_interpolation_formula) but they're much more complicated and so out of the scope of this tutorial.

With that in mind we're basically done, all that's left to do is alter our main function to use one of these interpolation functions:

```
audio = continuousToDiscrete(
    hermiteResample(
        discreteToContinuous(contents.samples),
        systemFrequency / static_cast<float>(contents.frequency)
    )
);
```

Here we use `hermiteResample` to double the number of samples, converting from 24,000 Hz to 48,000 Hz, and making our audio sound normal again. If you open the application you should hear the same music as usual, in spite of the fact that the underlying file is actually 24,000 Hz.
