Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 4: Localization, Panning and Falloff

Now it's finally time to add some localization to our sound (localization in this context refers to the ability of the human brain to pinpoint the origin of the sound in 3D space, not translating the sound into different languages). Before we get to actually doing this though I want to do a quick overview of localization as a topic. Human brains localize the source of a sound using a variety of cues. There are essentially two main components to localization: determining the direction the sound is coming from and determining how far away it is. To determine where the sound is coming from the human brain uses the following cues:

- **Interaural Time Difference (ITD):** As the two ears on a human head are in slightly different places a sound originating from anywhere off the centerline will arrive at one ear before it arrives at the other, and remain very slightly out of sync for its entire duration.
- **Interaural Intensity Difference (IID):** Again because the two ears on a human head are in different places a sound originating from anywhere off the centerline will have lost slightly more energy (e.g. it will be quieter) when it arrives at the more distant ear.
- **Head-Related Impulse Response (HRIR):** The shape of a human's head and ears interfere with incoming sounds in complex ways, affecting some frequencies more than others. The ear facing away from an off-center sound source will hear that sound as muffled due to the head obstructing it and the human ear is not symmetrical so a sound coming from directly ahead will be distorted differently than one originating directly behind the listener.

To determine distance the following cues are used:

- **Attenuation:** A sound originating from a more distant source will obviously be quieter than the same sound originating from a closer one. If you're familiar with the sound and its volume (e.g. the beeping of your microwave or a sound that plays often in a game) then you can judge the distance based on the currently observed volume.
- **Atmosphere-Related Impulse Response (ARIR):** As sound travels through the air to reach the listener parts of it are absorbed by the air in between. Much like interference from the human head and ears this isn't distributed equally across all frequencies but affects some more than others. The result is that distant sounds are muffled as high frequencies are absorbed and low frequencies remain, with the effect becoming more pronounced the further away the source of the sound.
- **"Flash-to-Bang" Time Delay (FBTD):** If you are looking at the source of a very distant sound, such as a lighting strike, there will be a delay between the visual information arriving at the speed of light (~300,000,000 m/s) and the sound arriving at the speed of sound (~343 m/s).
- **Initial Time Delay Gap (ITGD):** For close sounds the source-to-listener path and the source-to-reverb-surface-to-listener path are very different lengths (with the latter usually being much longer) while for distant sounds they are very similar.

We're going to be simulating all of these effects eventually but a lot of them are going to require significant groundwork before we can get them working, so to start with we're going to be implementing only the most basic. For determining the direction of the sound the most basic option available to us is **stereo panning**. Stereo panning is essentially a crude approximation of IID and HRIR. It makes the sound quieter in the ear facing away from the sound source and louder in the ear facing towards it. This approach has a couple of pros and cons:

- **Con:** It only provides information about sounds in 2D space: moving the sound source up or down will have no effect on what the listener hears.
- **Con:** It also doesn't distinguish between sounds directly in front of and directly behind the listener.
- **Pro:** It's very cheap and easy to calculate.
- **Pro:** It can be more suitable for users using speakers rather than headphones. When using speakers the sound from the game travels through the air and interacts with the listener's head and ears itself, and both ears can hear both channels (vs. when using headphones where the left ear can only hear the left channel and vice versa). This means a simulated HRIR tends not to work with this setup and instead just results in the sound sounding muddied and bad.

But direction isn't everything, we also need a way of judging distance, for this we'll use basic **spherical falloff**. This is a fairly accurate simulation of the attenuation due to distance we mentioned above. Unlike stereo panning we'll continue using it for the entirety of the tutorial series, but we will be adding extra effects to make distance reckoning better in future.

A note on the code: from this tutorial onwards I'm going to start moving parts of the code into header files lest the changes we're making be drowned in a sea of boilerplate functions from past tutorial parts.

Starting in our main function we're going to load the mono version of our test music rather the stereo version we used previously:

```
WAVContents contents = loadWAV("../../res/music-mono.wav");
```

This is normal for localized sounds because stereo sounds already contain localization. The guitar in a piece of music might be slightly more prominent in the right channel while the drums are slightly more prominent in the left. We want to start with a mono sound instead so we can dynamically alter the localization at runtime to match our game's 3D scene. This doesn't mean you'll never use stereo sounds in your game. There are a few types of sound that are usually played without localization: UI sounds, music, dialogue delivered over a radio in the player character's helmet, etc. These can still be stereo, but everything that originates from a position in the world should be mono. To support this I've also changed the WAV loading function slightly to expect mono audio rather than audio that matches the system channel count:

```
assert(formatHeader.channelCount == 1);
```

Next I've added a few hundred lines of boilerplate adding two-dimensional and three-dimensional vector types which will be necessary to describe our 3D scene using linear algebra. If you're using a game engine of any kind it almost certainly provides equivalents for these types and you're probably used to using them already. Using linear algebra to represent 3D space is a complex topic that could fill several tutorial series on its own but I'll do my best to provide a quick overview of the system I'm using. The listener is assumed to be sitting at the world origin `(0, 0, 0)`. The space is orientated so +X is right, +Y is forward and +Z is up. Positive rotations are clockwise. This means a sound source position of `(3, 0, 0)` would mean the sound source is 3 meters to the listener's right. `(-1, -1, 0)` puts the sound source behind the user's left ear, while `(0, 0, 1)` would put it directly overhead.

Armed with this we can replace our old "main loop", which did nothing but sleep for 30 seconds, with a crude facsimile of a real game loop:

```
while (currentTime() - startTime < chrono::seconds(30))
{
    this_thread::sleep_for(chrono::milliseconds(20));

    float elapsed = (currentTime() - startTime).count() / 1'000'000.0f;

    sourcePosition = vec3(rotate(sourceStartPosition, tau * elapsed * 0.1f) * elapsed * 0.02f, 0);
}
```

50 times a second we check how long it's been since the "game" started and use this to calculate the position of the sound source. We calculate it as a 2D position initially and then add the Z component at the end because it makes the rotation math easier and with our basic panning setup we can't hear differences in the height of the sound source anyway. The sound source starts out directly in front of the user (because `sourceStartPosition` is `(0, 1)`) and revolves around the head in a clockwise direction, making one full rotation every 10 seconds. At the same time the sound source gets steadily further away, receding by 1 meter every 50 seconds.

In our `onOutput` function we read the position generated by our main loop and use this to localize the sound. SDL requests a number of bytes of stereo output so we need to convert this to a number of samples per channel and create a buffer to hold that many samples:

```
int requestedSamplesPerChannel = outputSize / systemChannels / sizeof(int16_t);

vector<int16_t> monoDiscrete(requestedSamplesPerChannel, 0);
```

Next we read these samples from our preloaded music:

```
// All sizes are in bytes
auto input = reinterpret_cast<const uint8_t*>(audio.data());
int inputSize = audio.size() * sizeof(int16_t);
int remainingSize = inputSize - readHead;
int requestedSize = outputSize / systemChannels;
int readSize = min(requestedSize, remainingSize);
memcpy(monoDiscrete.data(), input + readHead, readSize);
readHead += readSize;
```

This might look complicated but really all we're doing is reading either however many samples are left in the audio sample or however many samples SDL requested, whichever is fewer. Now that we have our audio chunk we can do some processing on it and finally create our first localization effect! To do this we first convert the samples to continuous (e.g. floating point) format using the function we discussed in part 2:

```
vector<float> mono = discreteToContinuous(monoDiscrete);
```

Then we compute what direction the sound is in relative to the listener and how far away it is:

```
float distance = listenerPosition.distanceTo(sourcePosition);
vec3 direction = listenerPosition.directionTo(sourcePosition);
```

Then we compute the spherical falloff which will make the sound get quieter the further away the source is:

```
float falloff = 1 / pow(1 + distance, 2);
```

We divide by `pow(1 + distance, 2)` here because if the source and the listener are in exactly the same place we want the result to be 1 (e.g. no reduction in volume) rather than a math error from trying to divide by zero. Now we compute the 2D angle between the right direction and the direction to the sound source:

```
float angle = angleBetween(direction.toVec2().normalize(), rightDir) / 2;
```

The `angleBetween` function produces an unsigned angle (e.g. it does not distinguish between clockwise and anti-clockwise) in the range 0 to π. A value of 0 indicates the sound source is directly aligned with the right ear while a value of π indicates it's directly aligned with the left. We'll then feed this angle into `sin` and `cos` for the left and right ears respectively to get the amplitudes to multiply those channels by, the only issue is the peaks of `sin` and `cos` are `π/2` apart, not π. To fix this we divide the result by 2 to get the angle into the correct range. You can see the functions graphed here to better understand why we do this:

![](res/sin-and-cos.png)

Again we're doing this in only two dimensions because this simplified approach to spatial sound has no use for the third dimension. Now that we have that value we can actually apply our effects like so:

```
vector<float> left(mono.size());

for (size_t i = 0; i < mono.size(); i++)
{
    left[i] = sin(angle) * mono[i] * falloff;
}

vector<float> right(mono.size());

for (size_t i = 0; i < mono.size(); i++)
{
    right[i] = cos(angle) * mono[i] * falloff;
}
```

Finally all that's left to do is to merge the two channels into one interleaved stereo stream, convert it back to the discrete sample format (e.g. 16-bit signed integer) and hand it over to SDL for playback:

```
vector<float> stereo = joinStereo(left, right);

vector<int16_t> stereoDiscrete = continuousToDiscrete(stereo);

memcpy(output, stereoDiscrete.data(), stereoDiscrete.size() * sizeof(int16_t));
```

And that's it! If you open the example application you should hear the sound circling your head, starting from in front of you and heading clockwise, getting steadily quieter as it gets further away (this can be difficult to notice because the music itself has a very variable volume).
