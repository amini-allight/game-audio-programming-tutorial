Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 7: Saving WAV Files

I was originally going to do a tutorial about something else this week but then while developing that tutorial I realized I needed a function to save WAV files out of the program as well as loading them into it. So I decided to do a tutorial about that first. Saving to WAV isn't that useful for an actual finished game (I can't think of any games that export audio to the disk off the top of my head, although I'm sure some exist) but it is absolutely invaluable for developing game audio systems. There will be times when your audio system creates pops and crackles which indicate something is wrong. Those sounds could be caused by a variety of different distortions of the waveform such as clipping (where the sound overflows above the max value causing it to either be clamped to the max value or wrap around to the minimum value) or gaps in the stream (where several zeroes have been inserted). It can be very difficult to distinguish between these different sounds by ear so it's helpful to output the waveform into a WAV file so you can use a tool like [Audacity](https://www.audacityteam.org/) to examine it down to the level of individual samples and diagnose the issue.

We're going to define a new function called `saveWAV` which is basically a mirror image of `loadWAV`. The function takes a path and a `WAVContents` struct and saves the contents into a file at the specified path. **If the specified file already exists it will be overwritten** so keep that in mind when playing around with this.

```
inline void saveWAV(const string& path, const WAVContents& contents)
```

As a quick refresher `WAVContents` contains three fields: `samples` which is an array of 16-bit signed integer samples, `channels` which is the number of channels and `frequency` which is the sampling frequency. We start by opening the file for writing and checking it's really open:

```
FILE* file = fopen(path.c_str(), "wb");

assert(file);
```

Then we write the RIFF header. This header comes at the start of the file and informs us what type of file it is and how large it is.

```
RIFFHeader riffHeader{
    .headerID = { 'R', 'I', 'F', 'F' },
    .totalFileSize = static_cast<int32_t>(sizeof(RIFFHeader::fileType) + sizeof(WAVEFormatHeader) + sizeof(WAVEDataHeader) + contents.samples.size() * sizeof(int16_t)),
    .fileType = { 'W', 'A', 'V', 'E' }
};

fwrite(&riffHeader, sizeof(riffHeader), 1, file);
```

I'm using the C++20 feature [aggregate initialization](https://en.cppreference.com/w/cpp/language/aggregate_initialization) here because it allows me to easily fill character arrays with their correct values. This feature has been in C since C99 and has been supported by many C++ compilers for years via compiler extensions, but only officially became part of the C++ standard in 2020 so it's possible your compiler won't support it. If that's the case you can rewrite this code like so:

```
RIFFHeader riffHeader{};
riffHeader.headerID[0] = 'R';
riffHeader.headerID[1] = 'I';
riffHeader.headerID[2] = 'F';
riffHeader.headerID[3] = 'F';
riffHeader.totalFileSize = static_cast<int32_t>(sizeof(RIFFHeader::fileType) + sizeof(WAVEFormatHeader) + sizeof(WAVEDataHeader) + contents.samples.size() * sizeof(int16_t));
riffHeader.fileType[0] = 'W';
riffHeader.fileType[1] = 'A';
riffHeader.fileType[2] = 'V';
riffHeader.fileType[3] = 'E';

fwrite(&riffHeader, sizeof(riffHeader), 1, file);
```

The structure starts with a four-letter code indicating what type of file this is, which will always be "RIFF" for a WAV file. Next comes a size field. There are several of these throughout a WAV file, one in the RIFF header and then one in each of the chunk headers. The chunk header size fields store only the size of that chunk while the RIFF header's size field is special and stores the size of the entire file. In both cases this size is defined as the _size left to read_. By "size left to read" I mean the size without any fields that precede the size field, including the size field itself. You can see this in how we calculate this size. We add together the size of the format header, the size of the data header, the size of the data and the size of the only field in the RIFF header that hasn't yet been read, which is `RIFFHeader::fileType`. Speaking of `RIFFHeader::fileType`, this is a special four-letter code that tells us what kind of RIFF file this actually is. In our case we're working with WAV files so this will always be "WAVE".

Finally as a reminder: these header structs are defined with `#pragma pack(1)` to force the fields to be packed tightly together, as required by the WAV format. If you're unfamiliar with this idea [this tutorial](https://www.joshcaratelli.com/blog/struct-packing) can bring you up to speed.

Moving on we have two chunks to output, the format chunk and the data chunk. We can technically output these in any order but the format chunk typically comes first. The format chunk is just a fixed-size block of fields, like so:

```
WAVEFormatHeader formatHeader{
    .headerID = { 'f', 'm', 't', ' ' },
    .sectionSize = static_cast<int32_t>(sizeof(WAVEFormatHeader) - (sizeof(WAVEFormatHeader::headerID) + sizeof(WAVEFormatHeader::sectionSize))),
    .audioFormat = 1,
    .channelCount = static_cast<int16_t>(contents.channels),
    .sampleRate = contents.frequency,
    .byteRate = static_cast<int32_t>(contents.frequency * contents.channels * sizeof(int16_t)),
    .blockAlign = static_cast<int16_t>(contents.channels * sizeof(int16_t)),
    .bitsPerSample = sizeof(int16_t) * 8
};

fwrite(&formatHeader, sizeof(formatHeader), 1, file);
```

It starts with the four letter code "fmt " to indicate that this is a format chunk followed by a `sectionSize` calculated as above. Following those two standard fields are the actual content of the format chunk: six special fields that describe the audio format:

- `audioFormat`: How the samples should be interpreted. `1` means PCM.
- `channelCount`: The number of channels.
- `sampleRate`: The sampling frequency.
- `byteRate`: The number of bytes per second in the audio. This is the sampling frequency × the number of channels × the number of bytes per sample.
- `blockAlign`: The size of a "frame" in bytes. This is the size of a sample in bytes × the number of channels.
- `bitsPerSample`: The size of a sample in bits.

Now comes the data chunk:

```
WAVEDataHeader dataHeader{
    .headerID = { 'd', 'a', 't', 'a' },
    .sectionSize = static_cast<int32_t>(contents.samples.size() * sizeof(int16_t))
};

fwrite(&dataHeader, sizeof(dataHeader), 1, file);

fwrite(contents.samples.data(), sizeof(int16_t), contents.samples.size(), file);
```

Again we have the first two fields recording the size of the section and the special four letter code to identify the chunk, which this time is "data" to indicate this is the data chunk. After the header we write out our entire block of samples to serve as the body of this chunk.

Having done that we just close the file:

```
fclose(file);
```

Now we can return to our main file. We're going to add a new variable:

```
static vector<int16_t> audioToWAV;
```

And then whenever we have audio to give to SDL in our `onOutput` function we're also going to save those processed samples into this buffer:

```
audioToWAV.insert(audioToWAV.end(), stereoDiscrete.begin(), stereoDiscrete.end());
```

Then just before we exit at the end of our `main` function we'll use our new `saveWAV` function to write the audio we stored in this buffer to the disk.

```
saveWAV("out.wav", { systemChannels, systemFrequency, audioToWAV });
```

And that's it. If you let the example program run for its full 10 seconds it will exit and then you can open `out.wav` in your favorite audio player to listen to the recording of the output or open it in a tool like Audacity to perform further analysis.
