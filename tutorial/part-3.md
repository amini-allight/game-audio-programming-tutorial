Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 3: Loading WAV Files

Generating basic tones is all well and good but they have a few shortcomings. For one they're not particularly pleasant to listen to and for two their simplicity makes effects less obvious when applied to them. Is that tone really "muffled" when it's occluded by the wall? It just sounds like another slightly different tone. We can fix this by using a piece of music, human speech or a sound effect in place of a tone. I've picked a piece of music because I find effects are most obvious with music (e.g. play back a sound effect or human speech at the wrong sample rate and it will sound almost normal, do it with music and it will immediately sound strange).

The piece of music I'm going to be using throughout this tutorial is a performance of the first movement of Ludwig van Beethoven's Symphony No. 5 in C Minor, Op. 67 performed by Davis High School Symphony Orchestra. I'm using this because both the composition and the recording of the performance are in the public domain so I don't have to worry about including it in the files of this CC0-licensed tutorial.

But to actually play this music we need to load it from a file. There are a lot of different audio formats we could choose from here but for games specifically there are a number of concerns we need to balance, they are:

- **Disk usage:** We can just store audio uncompressed on the disk if we want but that can quickly get extremely large depending on how much audio your game has. The Witcher 3 reportedly has 300 hours of dialogue which at 16-bit 48,000 Hz mono that would come out to 96 GiB of uncompressed audio, and that's without sound effects or music (I don't know what format CD Projekt Red actually used, this is just an example).
- **Decompression speed:** We can instead store the audio as compressed, saving disk space, but then we have to decompress it before we can play it which costs CPU time. Very often the better the compression ratio you achieve the slower decompression will be. CPU time is often at a premium in games and if we don't decompress fast enough the sound will be late.
- **RAM usage:** To stop the sound being late we can decompress it ahead of time (e.g. when loading the level) and then keep it uncompressed in RAM read for instant playback. But where 96 GiB will probably fit on most storage devices nowadays it won't fit in the average PC or console's RAM meaning you need to be able to tell with a high degree of certainty which sounds are going to play soon and specifically prepare those.

Traditionally the game industry has often solved this balancing act by either degrading quality significantly or relying on expensive patent-encumbered proprietary compression algorithms and later on in this tutorial series we'll be discussing an open source format designed to solve this problem once and for all. But right now we're just going to be taking the first option: storing audio uncompressed on the disk. This is what WAV does and that makes it super simple to save and load, perfect for our purposes.

The WAV format is fairly standard as binary file formats go, it consists of an initial header followed by a series of sections each starting with a header and containing a mixture of pre-defined and user-defined fields. Here are the headers, defined as C++ structs:

```
#pragma pack(1)
struct RIFFHeader
{
    char headerID[4];
    int32_t totalFileSize;
    char fileType[4];
};

struct WAVEFormatHeader
{
    char headerID[4];
    int32_t sectionSize;
    int16_t audioFormat;
    int16_t channelCount;
    int32_t sampleRate;
    int32_t byteRate;
    int16_t blockAlign;
    int16_t bitsPerSample;
};

struct WAVEDataHeader
{
    char headerID[4];
    int32_t sectionSize;
};

struct GenericHeader
{
    char headerID[4];
    int32_t sectionSize;
};
#pragma pack()
```

Note I'm using `#pragma pack(1)` here to make sure all the fields are packed tightly together because this is the way WAV files are laid out. If you aren't familiar with this idea [this tutorial](https://www.joshcaratelli.com/blog/struct-packing) can bring you up to speed. Our function signature is gonna look like this:

```
static WAVContents loadWAV(const string& path);
```

Taking the path to a WAV file as an argument and returning a `WAVContents` structure which we've defined like so:

```
struct WAVContents
{
    int channels;
    int frequency;
    vector<int16_t> samples;
};
```

WAV files always contain uncompressed PCM data in the typical interleaved layout (if not mono) but they can contain a variety of different channel counts, frequencies and even sample sizes (e.g. 8-bit integer samples rather than 16-bit). To support this our function returns this structure containing not only the samples but also some metadata about how to correctly interpret them, which we'll be using in future tutorial parts.

Our function begins with us opening the file and reading out the first header:

```
FILE* file = fopen(path.c_str(), "rb");

assert(file);

// RIFF header
RIFFHeader riffHeader{};
result = fread(&riffHeader, sizeof(riffHeader), 1, file);

assert(result == 1);
assert(string(riffHeader.headerID, sizeof(riffHeader.headerID)) == "RIFF");
assert(string(riffHeader.fileType, sizeof(riffHeader.fileType)) == "WAVE");
```

This header just informs us what type of file this is, so we check the read succeeded then check that it really is a WAV file. Now we need to read out the other sections. These can be in any order and there can be metadata sections mixed in so we need to loop through looking for headers and figuring out which type we've found:

```
while (true)
{
    GenericHeader genericHeader{};
    result = fread(&genericHeader, sizeof(genericHeader), 1, file);

    if (result != 1)
    {
        break;
    }

    fseek(file, -sizeof(genericHeader), SEEK_CUR);

    string headerID(genericHeader.headerID, sizeof(genericHeader.headerID));

    // Format section
    if (headerID == "fmt ")
    {
        WAVEFormatHeader formatHeader{};
        result = fread(&formatHeader, sizeof(formatHeader), 1, file);

        assert(result == 1);
        assert(formatHeader.sampleRate == systemFrequency);
        assert(formatHeader.channelCount == systemChannels);
        assert(formatHeader.bitsPerSample == 16);

        contents.frequency = formatHeader.sampleRate;
        contents.channels = formatHeader.channelCount;
    }
    // Data section
    else if (headerID == "data")
    {
        WAVEDataHeader dataHeader{};
        result = fread(&dataHeader, sizeof(dataHeader), 1, file);

        assert(result == 1);

        contents.samples = vector<int16_t>(dataHeader.sectionSize / sizeof(int16_t), 0);
        result = fread(contents.samples.data(), 1, dataHeader.sectionSize, file);

        assert(result == dataHeader.sectionSize);
    }
    else
    {
        fseek(file, sizeof(genericHeader) + genericHeader.sectionSize, SEEK_CUR);
    }
}
```

We start out by reading the first two fields that all headers share, encapsulated here by the `GenericHeader` type. The first field is a four-character code that indicates which type of section this is and the second is the length of the section in bytes, minus the length of the these first two fields. If we fail to read a header entirely that probably means we've reached the end of the file and should exit the loop. If we get a valid read we reset the file position back to the start of the header because we'll be reading these two fields again as the start of our `WAVEFormatHeader` and `WAVEDataHeader` types.

If we get a format header we read it out into a structure and confirm the read succeeded. Then for simplicity's sake we make sure the WAV file matches our audio system parameters: 16-bit samples, 2 channels and a sampling rate of 48,000 Hz. If everything is okay we take note of the important values before moving on.

If we get a data header we read it out into a structure and confirm the read succeeded. Then we use the `sectionSize` field to determine how many samples there must be, allocate space for them in our vector and read them out, again confirming the read was successful before moving on.

If we get any other kind of header (usually this is metadata in a `LIST` or `INFO` header) we just move the file position forward to the end of the section and return to the start of the loop to try again.

Finally we do some basic checks to make sure we did actually find valid format and data headers during our traversal of the file, then close the file and return the result.

```
assert(contents.frequency != 0);
assert(contents.channels != 0);
assert(!contents.samples.empty());

fclose(file);

return contents;
```

Because we only accept WAV files in our usual audio format we can be sure that the data can be fed directly into SDL, meaning our main function now only has to contain this:

```
WAVContents contents = loadWAV("../../res/music-stereo.wav");

audio = contents.samples;

SDL_PauseAudioDevice(outputDevice, false);
this_thread::sleep_for(chrono::seconds(30));
```

And that's done. If you open the example you should hear the first 30 seconds of the sample music playing in both ears before the program exits. Next time we'll go over how exactly you convert between audio formats, allowing us to support WAV files that don't perfectly fit our parameters alongside those that do.
