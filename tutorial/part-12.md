Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, (this part is an exception and has a second dependency, opus) which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. You will need to set up the opus dependency yourself. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 12: Data Compression for Voice Chat

In the last tutorial we covered the basics of audio data compression and how you would go about using two different audio codecs, [Opus](https://opus-codec.org/) and [QOA](https://qoaformat.org/), to load audio from files on the disk. What we didn't cover was how you would use audio compression to implement voice chat. All the same concepts from the previous tutorial apply here but instead of the dedicated decode-only file loading APIs we're going to be using the raw encode-decode APIs. I've chosen to use Opus for this because it's battle tested in VoIP applications, it has a special voice mode designed specifically to preserve human speech and I have lots of experience using it for this. If you want to use QOA however all the same principles should apply: I've used many different encoders in the past and they all tend to have similar APIs. Much like in the last part we're going to need an external library to help us with the complicated business of encoding and decoding. Last time that was opusfile but this time it's the main [opus](https://github.com/xiph/opus) library. On Linux, MacOS, BSD, etc. you should be able to acquire this from your system package manager (or [Homebrew](https://brew.sh/) in MacOS' case) but unfortunately it wasn't available in the [Scoop](https://scoop.sh/) package repositories so on Windows you'll have to source it yourself.

Encoders and decoders work similarly to everything else in the audio world, operating on a series of discrete chunks of audio. These encoders and decoders are represented by opaque software objects because they themselves might have state (e.g. the content of block #1 might effect how block #2 is encoded): we're going to wrap these objects in classes of our own to make them easier to use. For Opus specifically there's an extra consideration: only certain block sizes are permitted. At a sampling rate of 48,000 Hz (which you may recall is essentially mandatory for Opus) the allowed values are 120, 240, 480, 960, 1920 or 2880 samples per channel. Chunk sizes elsewhere in our application may instead need to be powers of two (in order to facilitate Fourier transforms) so you'll probably need a small delay queue somewhere to convert between the two sizes but compared to network latency this shouldn't be a big deal. Also worth noting is that for chunk sizes smaller than 10 ms (that's 480 samples per channel at 48,000 Hz) the codec will be unable to use some of its features, resulting in a worse result (e.g. a larger compressed size and/or worse quality).

Starting with the encoder the class outline looks like this:

```
class VoiceEncoder
{
public:
    VoiceEncoder(int channels, int frequency, int bitrate);
    VoiceEncoder(const VoiceEncoder& rhs) = delete;
    VoiceEncoder(VoiceEncoder&& rhs) = delete;
    ~VoiceEncoder();

    VoiceEncoder& operator=(const VoiceEncoder& rhs) = delete;
    VoiceEncoder& operator=(VoiceEncoder&& rhs) = delete;

    vector<uint8_t> encode(const vector<int16_t>& samples);

private:
    int channels;
    OpusEncoder* encoder;
};
```

We have a constructor that sets up the encoder, a destructor that cleans it up and an `encode` function that does the actual encoding. The only state the class stores are a handle to the opaque encoder object and the number of channels requested by the instantiating code. The other four declarations serve to make this class non-copyable and non-movable, following the [rule of five](https://en.cppreference.com/w/cpp/language/rule_of_three). This prevents for example code like this:

```
void myFunction()
{
    VoiceEncoder encoder(2, 48000);
    VoiceEncoder encoder2 = encoder;
    return;
}
```

Where the user makes a copy of the `VoiceEncoder` leading to the destructor being called twice, the second time attempting to destroy the opaque handle which has already been destroyed and so causing a segmentation fault. Moving onto the class implementation we have our constructor:

```
VoiceEncoder(int channels, int frequency, int bitrate)
    : channels(channels)
{
    int status;

    encoder = opus_encoder_create(frequency, channels, OPUS_APPLICATION_VOIP, &status);

    assert(encoder);
    assert(status == OPUS_OK);

    status = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(bitrate));

    assert(status == OPUS_OK);
}
```

This calls the `opus_encoder_create` function with four arguments: the sampling frequency of the audio received from the user's microphone, the number of channels in the audio received from the user's microphone, an enum which describes what mode we'd like the encoder to use and a secondary return value used to communicate a status code. The enum here can be one of the following values:

- `OPUS_APPLICATION_VOIP`: This is for encoding voice, for when you want to prioritize maximum intelligibility of human speech.
- `OPUS_APPLICATION_AUDIO`: This is for general-purpose high fidelity audio, for when you want to preserve the input signal as best as possible.
- `OPUS_APPLICATION_RESTRICTED_LOWDELAY`: This is for when you require the absolute lowest possible encoding latency, for example in a cloud gaming system.

Play around with these as you like but `OPUS_APPLICATION_VOIP` should be a sensible default for in-game voice chat use cases. After this we just set the bitrate via the `opus_encoder_ctl` function and the `OPUS_SET_BITRATE` macro. There are lots of other things you can customize with this function, such as enabling variable bitrate behavior where the encoder dynamically adjusts the bitrate depending on the complexity of the input sound. These get quite technical so I won't go through all of them but you can find descriptions of them on [this page](https://www.opus-codec.org/docs/html_api/group__encoderctls.html) of the Opus API documentation.

There's very little to say about the destructor but here it is anyway:

```
~VoiceEncoder()
{
    opus_encoder_destroy(encoder);
}
```

Finally we need our encoding function, which looks like this:

```
vector<uint8_t> encode(const vector<int16_t>& samples)
{
    vector<uint8_t> encoded(samples.size() * sizeof(int16_t), 0);

    int status = opus_encode(
        encoder,
        samples.data(),
        samples.size() / channels,
        encoded.data(),
        encoded.size()
    );

    assert(status >= 0);

    return { encoded.begin(), encoded.begin() + status };
}
```

I'm assuming here that the worst-case size of the output data is exactly the same size as the input data (e.g. no useful compression occurred). I wasn't able to find any confirmation that this is the canonical way to do this but it's always worked for me in the past and makes logical sense. If you have issues with it or know more please let me know and I'll update this tutorial to reflect that. In any case the Opus documentation specifies that the size of the output buffer can be used to control the instantaneous bitrate, so even if Opus would like to exceed this limit it will refrain from doing so in practice.

This function focuses on the call to `opus_encode` which takes five arguments: the encoder, the samples and the number of them per channel (this is why we stored the channel count earlier), the encoded data and its size in bytes. The function returns negative error codes on failure and otherwise returns the number of bytes of encoded data produced, which I use in the last line to truncate the data buffer to only the relevant section.

Finally don't forget that the blocks you supply to this function must be one of the supported Opus encoding chunk sizes I mentioned above and they must be above 10 ms in length if you want to avail of the full suite of encoding features.

Now the decoder, the class outline is similar:

```
class VoiceDecoder
{
public:
    VoiceDecoder(int channels, int frequency);
    VoiceDecoder(const VoiceDecoder& rhs) = delete;
    VoiceDecoder(VoiceDecoder&& rhs) = delete;
    ~VoiceDecoder();

    VoiceDecoder& operator=(const VoiceDecoder& rhs) = delete;
    VoiceDecoder& operator=(VoiceDecoder&& rhs) = delete;

    vector<int16_t> decode(const vector<uint8_t>& encoded);

private:
    int channels;
    OpusDecoder* decoder;
};
```

This is mostly like the encoder with one notable difference: there is no longer a bitrate setting because the encoder chooses the bitrate, the decoder learns the bitrate it will be forced to deal with from the stream it's fed. The constructor is a simpler version of the previous one:

```
VoiceDecoder(int channels, int frequency)
    : channels(channels)
{
    int status;

    decoder = opus_decoder_create(frequency, channels, &status);

    assert(decoder);
    assert(status == OPUS_OK);
}
```

And the predictable destructor:

```
~VoiceDecoder()
{
    opus_decoder_destroy(decoder);
}
```

The decoding function is likewise similar:

```
vector<int16_t> decode(const vector<uint8_t>& encoded)
{
    int samplesPerChannel = opus_decoder_get_nb_samples(decoder, encoded.data(), encoded.size());

    assert(samplesPerChannel >= 0);

    vector<int16_t> samples(samplesPerChannel * channels, 0);

    int status = opus_decode(
        decoder,
        encoded.data(),
        encoded.size(),
        samples.data(),
        samples.size() / channels,
        false
    );

    assert(status >= 0);

    return { samples.begin(), samples.begin() + status * channels };
}
```

First we ask Opus how many samples per channel are expected in the decoded packet. Then we allocate a buffer of that size and use `opus_decode` to do the decode. This function takes six arguments: our decoder, the encoded data and its size in bytes, the output samples and the number of them per channel and a flag for forward error correction. We'll come back to this flag in the next tutorial part, just leave it false for now. The return value of this function is either a negative error code or the number of meaningful samples per channel that were decoded. We use this to truncate the buffer to only the meaningful part in the final line.

Now that we have our encoder and decoder we can apply them to some speech. In a future part we'll address capturing audio from the user's microphone but for now we'll settle for a file containing pre-recorded speech. The speech I have ended up using is the first minute of [this speech](https://www.youtube.com/watch?v=cTDln2f0GvQ) given by Barack Obama on January 10th 2009. I didn't pick this for political reasons. It is just remarkably hard to find audio of human speech that is both clear (any recording that is public domain due to its age is from 1923 or earlier and so has terrible audio quality by modern standards) and has a compatible permissive license (even Wikipedia article narrations have a ShareAlike license which is dubiously compatible with the CC0 used for this tutorial). This was literally the first thing I found that fit those two criteria.

To play this audio I've modified the main function of our example to contain this:

```
WAVContents contents = loadWAV("../../res/speech.wav");

constexpr int voiceChannels = 1;
constexpr int chunkSize = 2880 * voiceChannels;
constexpr int bitrate = 12000;

VoiceEncoder encoder(voiceChannels, systemFrequency, bitrate);
VoiceDecoder decoder(voiceChannels, systemFrequency);

for (size_t i = 0; i < ceil(contents.samples.size() / static_cast<float>(chunkSize)); i++)
{
    vector<int16_t> chunk(chunkSize, 0);

    memcpy(
        chunk.data(),
        contents.samples.data() + chunkSize * i,
        min<size_t>(chunkSize, contents.samples.size() - chunkSize * i) * sizeof(int16_t)
    );

    vector<int16_t> decoded = decoder.decode(encoder.encode(chunk));

    audio = join(audio, decoded);
}

SDL_PauseAudioDevice(outputDevice, false);

this_thread::sleep_for(chrono::seconds(30));
```

First we load the speech WAV, then we define some constants. Voice data will have 1 channel, it will be processed in chunks containing 2880 samples per channel, the maximum size allowed by Opus. The bitrate will be 12,000 bits per second. This is a very low bitrate and I've chosen it deliberately to make the effect of the compression noticeable but not overwhelming. You'll need to pick your own bitrate depending on the requirements of your game but it will probably be higher than this. Next we initialize our encoder and decoder with the requisite settings and start our encoding/decoding loop. We grab the audio one chunk at a time, encode it, immediately decode it again and then add it to the `audio` vector which our program will eventually play back through the speakers. Finally we unpause the audio system and sleep to allow the audio time to play, like usual.

If you run this you should hear the speech file played back at a lower quality than the original crisp audio, but still perfectly intelligible. Of course in a real voice chat application you would send the audio via the network connection in between encoding it and decoding it but this simplified version lets us explore the tradeoffs between stream size and perceived quality without all the complexity that a networking setup brings. Finally if you implement this over a real network **be aware of Denial of Service (DoS) attack vectors**. For example right now we have `assert(status >= 0)` in the decoder. This is fine for an example but in a real network situation a malicious modified client could easily exploit this by sending a deliberately malformed packet that will fail to decode, crashing the game for anyone who tries to decode it. Therefore in a real application it's important to be tolerant of errors like this and **never trust remote clients**. That's really it for this part, in the next part we'll look at how to deal with packet loss in our voice stream (e.g. if we send audio packets via UDP without any form of retransmission scheme in place).
