Hello and welcome to my game audio programming tutorial! Game audio is something I feel quite strongly about. Every time I see games push ever closer to perfect visual photorealism while audio remains stuck at playing canned sound effects with basic panning and distance falloff I sigh a little. To be clear I understand why this happens: visuals are simply much more obvious. Visuals come across in a trailer regardless of music blaring over the top, the video being muted or playing on laptop speakers three feet away. Audio meanwhile is subtle, "the best Foley work is Foley work the audience doesn't notice" is an often repeated adage for a reason. But audio is also vital. Have you ever wondered why it's so hard to walk in a group with your friends in a first-person game without people disappearing when you're not looking, compared with real life where this rarely happens? Half of that is you probably have 90 — 120° FOV in the game instead of 200 — 220° in real life, but the other half is sound. I can lie in my bed listening to the sounds in my house and know exactly who is doing what in which room. This level of audio situational awareness is rarely if ever available in games.

So when I was working on the sound system for my sandbox game LambdaMod I wanted to change this. Having done a rendering system with high dynamic range, dynamic lighting and automatic ambient occlusion I wanted to try my best to do the same for audio, producing a system with the best fully automatic spatialized sound I could create. When I started working on that system I discovered that, unlike for most rendering techniques, tutorials on how to do things were few and far between and I ended up relying on a mixture of my own intuition, reference books and signal processing experts I know personally to make progress. But I did eventually manage to make progress and now I've decided to turn that progress into a tutorial so anyone who comes this way again will have more to work from than I did.

Now a word on what this isn't. This isn't the opinion of a game audio programming professional. I have never worked in this role professionally. This tutorial isn't aimed at recreating industry standard techniques but rather replacing them with something forward looking. This comes with costs: my approach is significantly more performance and memory intensive than more traditional techniques. I'm also not a signal processing expert, just somebody who managed to struggle their way through it to get this working. If you _are_ a game audio or signal processing expert and you find something in here that's wrong please contact me on any of my socials and I'll do my best to correct it!

This tutorial expects you have a familiarity with C++ but no prior knowledge of audio programming specifically. The tutorial examples are cross-platform and have one dependency, SDL2, which is used to output audio to your speakers. To build the examples on Linux, BSD or MacOS you can just run the included `build.sh` script found in each example directory. To build on Windows you first need to run the `scoop-setup.ps1` PowerShell script found in the root directory of the project. This will install the [Scoop](https://scoop.sh/) package manager, then use it to install the GCC compiler and SDL2 library. You only needs to do this once. Once the dependencies are installed you can run the `build.ps1` script in an example's directory to build it. Also included is a `CMakeLists.txt` file if you'd prefer to build that way on any platform.

**Warning:** Some of the sounds we're going to be making are loud and unpleasant. Keep your system volume low.

## Part 10: Time-Domain Filters & Pass Filtering

So far we've implemented filters by transforming the sound into the frequency domain, filtering it, and then transforming it back again. But there is another way we can apply filters without ever having to leave the time domain, predictably called time-domain filtering. The concepts here are quite complex and there are a lot of them stacked up but I've done my best to make it as easy to understand as possible. Let's get started.

The way this is going to work is we're going to take our chunk of samples and "combine" it with a filter to produce the filtered output. The filter is expressed as an array of numbers of some length. These numbers are called coefficients. The way we "combine" these coefficients with our sample chunk is called [convolution](https://en.wikipedia.org/wiki/Convolution). This is like the Fourier transform in that you don't really need to understand how it works (I don't): you can just treat it as a black-box mathematical operations you pull out for certain situations. We'll come back to convolution and exactly how to do it later.

But where does the array of coefficients come from? It's a combination of two things. The first is the filter itself. The filter is represented by a function which returns a coefficient for each index in the array. In C++ this might look like this: `coefficients[n] = myFilter(n)`. What exactly the `myFilter` function does will depend on exactly what effect you want your filter to implement. The problem with filters like this is they are what's known as an [infinite impulse response](https://en.wikipedia.org/wiki/Infinite_impulse_response), generating coefficients forever, which makes them impossible to use in practice. To fix this we use a [window function](https://en.wikipedia.org/wiki/Window_function). This makes the filter less ideal (e.g. it introduces imperfections into its operation) but it's necessary to make them usable. The window function defines a length for our array of coefficients (which is really called a window) and also provides a second coefficient to multiply by the first to get the finished coefficient. These finished coefficients are also known as a [finite impulse response](https://en.wikipedia.org/wiki/Finite_impulse_response). In C++ this might look like this:

```
int m = windowSize();
vector<float> coefficients(m);

for (int n = 0; n < m; n++)
{
    coefficients[n] = window(n) * myFilter(n);
}
```

And there we have a finished filter. Now we just convolve that with our chunk and the filter has been applied. Now let's look at a more concrete example.

The filters we're going to be implementing are the [low-pass filter](https://en.wikipedia.org/wiki/Low-pass_filter) and the [high-pass filter](https://en.wikipedia.org/wiki/High-pass_filter). These are filters that remove or reduce strength of frequencies above or below a certain cutoff. A low-pass filter allows low frequencies to "pass" through unaltered while removing higher frequencies and a high-pass filter does exactly the opposite. It might be tempting to try implement these using the frequency domain: simply transform into the frequency domain and zero the lower frequencies or the upper ones then transform back to the time domain, right? Unfortunately what you end up doing here is implementing what's known as a "brick-wall filter". This is an ideal infinite filter (as described above) that perfectly removes all the frequencies above or below a certain frequency and leaves all other frequencies perfectly unchanged. In practice this filter cannot be implemented so when you try to play back your sound after doing this you'll encounter nasty artifacting. Instead we need to implement a filter with a [transition band](https://en.wikipedia.org/wiki/Transition_band), a region of frequencies that is neither completely removed nor completely preserved. The wider this transition band is the smaller the window of coefficients that will be needed but the less dramatic the effect of the filter will be. This is a tradeoff you will need to make depending on your needs. Convolution can be quite compute intensive so a smaller window of coefficients is desirable but a harder cutoff between low and high frequencies usually sounds better.

With that out of the way, here's our low pass filter:

```
inline float loPassFilter(size_t n, size_t m, float passFrequency, float stopFrequency)
{
    float transitionCenterFrequency = toNormalizedAngularFrequency((passFrequency + stopFrequency) / 2, systemFrequency);

    return (transitionCenterFrequency / pi) * sinc((transitionCenterFrequency * (n - static_cast<float>(m / 2))) / pi);
}
```

This is equivalent to our `myFilter` function in our simplified example before. The `n` parameter is the index of the coefficient to generate, the `m` parameter is the total size of the window and the `passFrequency` and `stopFrequency` define where the transition band will start and end. For example we might supply `1000` and `2000` to indicate that all frequencies below 1000 Hz should be fully preserved and all frequencies above 2000 Hz should be fully removed. You'll notice this function uses a helper function called `toNormalizedAngularFrequency`. Essentially what's going on here is values in Hz are meaningless in our world of samples and coefficients: they have to be somehow converted into a form relative to the base sampling frequency of our system. To achieve this we convert from Hz to a form known as [angular frequency](https://en.wikipedia.org/wiki/Angular_frequency) and then [normalize](https://en.wikipedia.org/wiki/Normalized_frequency_(signal_processing)) it. This function is defined like this:

```
inline float toNormalizedAngularFrequency(float frequency, float samplingFrequency)
{
    return (frequency / (samplingFrequency / 2)) * pi;
}
```

The other oddity you might notice here is our use of the [sinc](https://en.wikipedia.org/wiki/Sinc_function) function. This is another standard math function, similar to sin or cos. But unlike those examples sinc isn't available in our standard library so we'll need to define it ourselves. We define it like so:

```
inline float sinc(float x)
{
    if (x == 0)
    {
        return 1;
    }
    else
    {
        return sin(x * pi) / (x * pi);
    }
}
```

Strictly speaking what we're defining here is known as _normalized_ sinc. That's basically everything you need to know about the inner workings of the low-pass filter function. The high-pass filter is mostly the same:

```
inline float hiPassFilter(size_t n, size_t m, float passFrequency, float stopFrequency)
{
    float transitionCenterFrequency = toNormalizedAngularFrequency((passFrequency + stopFrequency) / 2, systemFrequency);

    return sinc(n - static_cast<float>(m / 2)) - (transitionCenterFrequency / pi) * sinc((transitionCenterFrequency * (n - static_cast<float>(m / 2))) / pi);
}
```

Next we need a window function. There are lots of window functions to choose from with different strengths and weaknesses but we'll be using the [Hamming window](https://en.wikipedia.org/wiki/Window_function#Hann_and_Hamming_windows) which has worked for me so far and is simple to implement. We determine the length of the window from the width of the transition band like so:

```
inline size_t hammingWindowSize(float passFrequency, float stopFrequency)
{
    return (8 * pi) / toNormalizedAngularFrequency(abs(passFrequency - stopFrequency), systemFrequency);
}
```

Then we generate the window coefficients like so:

```
inline float hammingWindow(size_t n, size_t m)
{
    return 0.54 - 0.46 * cos((2 * n * pi) / m);
}
```

This is all very simple math and again you don't really need to understand what it's doing, just how to use it. Finally we can put it all together and create our completed filters:

```
inline vector<float> hammingWindowedLoPass(float passFrequency, float stopFrequency)
{
    size_t m = hammingWindowSize(passFrequency, stopFrequency);

    vector<float> coefficients(m);

    for (size_t n = 0; n < m; n++)
    {
        coefficients[n] = hammingWindow(n, m) * loPassFilter(n, m, passFrequency, stopFrequency);
    }

    return coefficients;
}

inline vector<float> hammingWindowedHiPass(float passFrequency, float stopFrequency)
{
    size_t m = hammingWindowSize(passFrequency, stopFrequency);

    vector<float> coefficients(m);

    for (size_t n = 0; n < m; n++)
    {
        coefficients[n] = hammingWindow(n, m) * hiPassFilter(n, m, passFrequency, stopFrequency);
    }

    return coefficients;
}
```

These functions will each take a `passFrequency` and a `stopFrequency`, compute the necessary window length and then fill the window with coefficients made by multiplying the window function's coefficients with the filter function's coefficients. Now let's define a new variable to store our coefficients:

```
static vector<float> coefficients;
```


And add a call to this to our `main` function:

```
coefficients = hammingWindowedLoPass(1000, 2000);
```

We only need to generate the coefficients once for each configuration of the filter (e.g. each pair of `passFrequency` and `stopFrequency` you want to use) so we can just keep these and reuse them for every chunk. To actually apply them to the chunks we're going to need a convolution operator, which looks like this:

```
inline vector<float> convolve(const vector<float>& input, const vector<float>& coefficients)
{
    vector<float> output(input.size(), 0);

    for (int n = 0; n < output.size(); n++)
    {
        for (int m = min<int>(n, coefficients.size() - 1); m >= 0; m--)
        {
            output[n] += input[n - m] * coefficients[m];
        }
    }

    return output;
}
```

Keep in mind this is potentially a _very_ hot function. I've done some things here (e.g. using `std::vector::operator[]` rather than the slower `std::vector::at`) to try and make it faster but there's more that could be done and you should keep an eye on this function when optimizing your sound system. Then we apply the filter to our mono sound, before the other transformations:

```
mono = convolve(mono, coefficients);
```

And we're done! In the included example file I've removed the doppler shift and sound source motion from the earlier examples and instead placed the sound source right in front of the listener's head. I also switched back to music rather than a tone sound. All of these changes should make it easier for you to hear what these filters do. You should hear that low-pass muffles the sound, making it sound like it's distant or behind an obstacle, while high-pass makes the sound sound "tinny" and like it's coming through a radio or other device. You can try playing with different transition band locations and widths and see how it affects performance and sound quality.

These filters are the backbone of almost all effects we're going to be doing from here on out, but we'll discuss that in greater detail in the next part.
