cmake_minimum_required(VERSION 3.5)

add_executable(example example.cpp)

target_link_libraries(example SDL2)
