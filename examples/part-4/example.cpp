#include "generators.hpp"
#include "wav.hpp"
#include "tools.hpp"
#include "linear_algebra.hpp"

#include <thread>
#include <iostream>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

using namespace std;

constexpr vec2 sourceStartPosition = vec2(0, 1);
constexpr vec3 listenerPosition = vec3(0, 0, 0);
constexpr vec2 rightDir = vec2(1, 0);

static vector<int16_t> audio;
static size_t readHead = 0;
static vec3 sourcePosition = vec3(sourceStartPosition, 0);

static void onOutput(void* userdata, uint8_t* output, int outputSize)
{
    int requestedSamplesPerChannel = outputSize / systemChannels / sizeof(int16_t);

    vector<int16_t> monoDiscrete(requestedSamplesPerChannel, 0);

    // All sizes are in bytes
    auto input = reinterpret_cast<const uint8_t*>(audio.data());
    int inputSize = audio.size() * sizeof(int16_t);
    int remainingSize = inputSize - readHead;
    int requestedSize = outputSize / systemChannels;
    int readSize = min(requestedSize, remainingSize);
    memcpy(monoDiscrete.data(), input + readHead, readSize);
    readHead += readSize;

    vector<float> mono = discreteToContinuous(monoDiscrete);

    float distance = listenerPosition.distanceTo(sourcePosition);
    vec3 direction = listenerPosition.directionTo(sourcePosition);

    float falloff = 1 / pow(1 + distance, 2);
    float angle = angleBetween(direction.toVec2().normalize(), rightDir) / 2;

    vector<float> left(mono.size());

    for (size_t i = 0; i < mono.size(); i++)
    {
        left[i] = sin(angle) * mono[i] * falloff;
    }

    vector<float> right(mono.size());

    for (size_t i = 0; i < mono.size(); i++)
    {
        right[i] = cos(angle) * mono[i] * falloff;
    }

    vector<float> stereo = joinStereo(left, right);

    vector<int16_t> stereoDiscrete = continuousToDiscrete(stereo);

    memcpy(output, stereoDiscrete.data(), stereoDiscrete.size() * sizeof(int16_t));
}

int main(int, char**)
{
    int error = SDL_Init(SDL_INIT_AUDIO);

    if (error)
    {
        cerr << "Failed to initialize SDL audio: " << SDL_GetError() << endl;
        return 1;
    }

    SDL_AudioSpec outputDesired{};
    outputDesired.freq = systemFrequency;
    outputDesired.format = AUDIO_S16SYS;
    outputDesired.channels = systemChannels;
    outputDesired.samples = systemBufferSize;
    outputDesired.callback = onOutput;

    SDL_AudioSpec outputObtained{};

    int outputDevice = SDL_OpenAudioDevice(
        nullptr,
        false,
        &outputDesired,
        &outputObtained,
        0
    );;

    if (!outputDevice)
    {
        cerr << "Failed to open audio output device: " << SDL_GetError() << endl;
        return 1;
    }

    WAVContents contents = loadWAV("../../res/music-mono.wav");

    audio = contents.samples;

    chrono::microseconds startTime = currentTime();
    SDL_PauseAudioDevice(outputDevice, false);

    while (currentTime() - startTime < chrono::seconds(30))
    {
        this_thread::sleep_for(chrono::milliseconds(20));

        float elapsed = (currentTime() - startTime).count() / 1'000'000.0f;

        sourcePosition = vec3(rotate(sourceStartPosition, tau * elapsed * 0.1f) * elapsed * 0.02f, 0);
    }

    SDL_CloseAudioDevice(outputDevice);

    SDL_Quit();

    return 0;
}
