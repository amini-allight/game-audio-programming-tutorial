#pragma once

#include <cmath>
#include <ostream>

using namespace std;

struct vec2
{
    constexpr vec2()
        : x(0)
        , y(0)
    {

    }

    explicit constexpr vec2(float v)
        : x(v)
        , y(v)
    {

    }

    constexpr vec2(float x, float y)
        : x(x)
        , y(y)
    {

    }

    constexpr float dot(const vec2& rhs) const
    {
        return x*rhs.x + y*rhs.y;
    }

    constexpr vec2 normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        return *this / length();
    }

    constexpr float length() const
    {
        return sqrt(pow(x, 2) + pow(y, 2));
    }

    constexpr vec2 directionTo(const vec2& rhs) const
    {
        return (rhs - *this).normalize();
    }

    constexpr float distanceTo(const vec2& rhs) const
    {
        return (rhs - *this).length();
    }

    constexpr vec2 operator+() const
    {
        return *this;
    }

    constexpr vec2 operator-() const
    {
        return vec2(
            -x,
            -y
        );
    }

    constexpr vec2 operator+(const vec2& rhs) const
    {
        return vec2(
            x + rhs.x,
            y + rhs.y
        );
    }

    constexpr vec2 operator-(const vec2& rhs) const
    {
        return vec2(
            x - rhs.x,
            y - rhs.y
        );
    }

    constexpr vec2 operator*(const vec2& rhs) const
    {
        return vec2(
            x * rhs.x,
            y * rhs.y
        );
    }

    constexpr vec2 operator/(const vec2& rhs) const
    {
        return vec2(
            x / rhs.x,
            y / rhs.y
        );
    }

    constexpr vec2& operator+=(const vec2& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    constexpr vec2& operator-=(const vec2& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    constexpr vec2& operator*=(const vec2& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr vec2& operator/=(const vec2& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr vec2 operator+(float rhs) const
    {
        return *this + vec2(rhs);
    }

    constexpr vec2 operator-(float rhs) const
    {
        return *this - vec2(rhs);
    }

    constexpr vec2 operator*(float rhs) const
    {
        return *this * vec2(rhs);
    }

    constexpr vec2 operator/(float rhs) const
    {
        return *this / vec2(rhs);
    }

    constexpr vec2& operator+=(float rhs)
    {
        *this = *this + vec2(rhs);

        return *this;
    }

    constexpr vec2& operator-=(float rhs)
    {
        *this = *this - vec2(rhs);

        return *this;
    }

    constexpr vec2& operator*=(float rhs)
    {
        *this = *this * vec2(rhs);

        return *this;
    }

    constexpr vec2& operator/=(float rhs)
    {
        *this = *this / vec2(rhs);

        return *this;
    }

    constexpr bool operator==(const vec2& rhs)
    {
        return x == rhs.x &&
            y == rhs.y;
    }

    constexpr bool operator!=(const vec2& rhs)
    {
        return !(*this == rhs);
    }

    float x;
    float y;
};

inline ostream& operator<<(ostream& out, const vec2& v)
{
    out << "(" << v.x << ", " << v.y << ")";

    return out;
}

constexpr vec2 operator+(float lhs, const vec2& rhs)
{
    return vec2(lhs) + rhs;
}

constexpr vec2 operator-(float lhs, const vec2& rhs)
{
    return vec2(lhs) - rhs;
}

constexpr vec2 operator*(float lhs, const vec2& rhs)
{
    return vec2(lhs) * rhs;
}

constexpr vec2 operator/(float lhs, const vec2& rhs)
{
    return vec2(lhs) / rhs;
}

struct vec3
{
    constexpr vec3()
        : x(0)
        , y(0)
        , z(0)
    {

    }

    explicit constexpr vec3(float v)
        : x(v)
        , y(v)
        , z(v)
    {

    }

    constexpr vec3(float x, float y, float z)
        : x(x)
        , y(y)
        , z(z)
    {

    }

    constexpr vec3(const vec2& xy, float z)
        : x(xy.x)
        , y(xy.y)
        , z(z)
    {

    }

    constexpr float dot(const vec3& rhs) const
    {
        return x*rhs.x + y*rhs.y + z*rhs.z;
    }

    constexpr vec3 cross(const vec3& rhs) const
    {
        return vec3(
            y*rhs.z - rhs.y*z,
            z*rhs.x - rhs.z*x,
            x*rhs.y - rhs.x*y
        );
    }

    constexpr vec3 normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        return *this / length();
    }

    constexpr float length() const
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

    constexpr vec3 directionTo(const vec3& rhs) const
    {
        return (rhs - *this).normalize();
    }

    constexpr float distanceTo(const vec3& rhs) const
    {
        return (rhs - *this).length();
    }

    constexpr vec3 operator+() const
    {
        return *this;
    }

    constexpr vec3 operator-() const
    {
        return vec3(
            -x,
            -y,
            -z
        );
    }

    constexpr vec3 operator+(const vec3& rhs) const
    {
        return vec3(
            x + rhs.x,
            y + rhs.y,
            z + rhs.z
        );
    }

    constexpr vec3 operator-(const vec3& rhs) const
    {
        return vec3(
            x - rhs.x,
            y - rhs.y,
            z - rhs.z
        );
    }

    constexpr vec3 operator*(const vec3& rhs) const
    {
        return vec3(
            x * rhs.x,
            y * rhs.y,
            z * rhs.z
        );
    }

    constexpr vec3 operator/(const vec3& rhs) const
    {
        return vec3(
            x / rhs.x,
            y / rhs.y,
            z / rhs.z
        );
    }

    constexpr vec3& operator+=(const vec3& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    constexpr vec3& operator-=(const vec3& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    constexpr vec3& operator*=(const vec3& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr vec3& operator/=(const vec3& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr vec3 operator+(float rhs) const
    {
        return *this + vec3(rhs);
    }

    constexpr vec3 operator-(float rhs) const
    {
        return *this - vec3(rhs);
    }

    constexpr vec3 operator*(float rhs) const
    {
        return *this * vec3(rhs);
    }

    constexpr vec3 operator/(float rhs) const
    {
        return *this / vec3(rhs);
    }

    constexpr vec3& operator+=(float rhs)
    {
        *this = *this + vec3(rhs);

        return *this;
    }

    constexpr vec3& operator-=(float rhs)
    {
        *this = *this - vec3(rhs);

        return *this;
    }

    constexpr vec3& operator*=(float rhs)
    {
        *this = *this * vec3(rhs);

        return *this;
    }

    constexpr vec3& operator/=(float rhs)
    {
        *this = *this / vec3(rhs);

        return *this;
    }

    constexpr bool operator==(const vec3& rhs)
    {
        return x == rhs.x &&
            y == rhs.y &&
            z == rhs.z;
    }

    constexpr bool operator!=(const vec3& rhs)
    {
        return !(*this == rhs);
    }

    constexpr vec2 toVec2() const
    {
        return vec2(x, y);
    }

    float x;
    float y;
    float z;
};

inline ostream& operator<<(ostream& out, const vec3& v)
{
    out << "(" << v.x << ", " << v.y << ", " << v.z << ")";

    return out;
}

constexpr vec3 operator+(float lhs, const vec3& rhs)
{
    return vec3(lhs) + rhs;
}

constexpr vec3 operator-(float lhs, const vec3& rhs)
{
    return vec3(lhs) - rhs;
}

constexpr vec3 operator*(float lhs, const vec3& rhs)
{
    return vec3(lhs) * rhs;
}

constexpr vec3 operator/(float lhs, const vec3& rhs)
{
    return vec3(lhs) / rhs;
}

constexpr vec2 rotate(vec2 point, float angle)
{
    float s = sin(-angle);
    float c = cos(-angle);

    float x = point.x * c - point.y * s;
    float y = point.x * s + point.y * c;

    return vec2(x, y);
}

constexpr float angleBetween(vec2 p, vec2 q)
{
    return acos(p.dot(q));
}
