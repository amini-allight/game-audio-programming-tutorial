#pragma once

#include "constants.hpp"

inline float toNormalizedAngularFrequency(float frequency, float samplingFrequency)
{
    return (frequency / (samplingFrequency / 2)) * pi;
}

inline float fromNormalizedAngularFrequency(float fraction, float samplingFrequency)
{
    return (fraction / pi) * (samplingFrequency / 2);
}
