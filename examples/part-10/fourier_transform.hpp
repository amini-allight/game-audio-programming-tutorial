#pragma once

#include "constants.hpp"
#include "tools.hpp"

#include <vector>
#include <complex>
#include <valarray>
#include <cassert>

using namespace std;

inline void fastFourierTransform(valarray<complex<float>>& x)
{
    size_t n = x.size();

    if (n <= 1)
    {
        return;
    }

    valarray<complex<float>> even = x[slice(0, n / 2, 2)];
    valarray<complex<float>> odd = x[slice(1, n / 2, 2)];

    fastFourierTransform(even);
    fastFourierTransform(odd);

    for (size_t k = 0; k < n / 2; k++)
    {
        complex<float> t = polar<float>(1, -2 * pi * k / n) * odd[k];

        x[k] = even[k] + t;
        x[k + (n / 2)] = even[k] - t;
    }
}

inline valarray<complex<float>> fft(const vector<float>& samples)
{
    assert(isPowerOfTwo(samples.size()));

    valarray<complex<float>> x(samples.size());

    for (size_t i = 0; i < samples.size(); i++)
    {
        x[i] = samples[i];
    }

    fastFourierTransform(x);

    return x;
}

inline vector<float> ifft(valarray<complex<float>> x)
{
    for (complex<float>& v : x)
    {
        v = conj(v);
    }

    fastFourierTransform(x);

    for (complex<float>& v : x)
    {
        v = conj(v);

        v /= x.size();
    }

    vector<float> samples(x.size());

    for (size_t i = 0; i < x.size(); i++)
    {
        samples[i] = x[i].real();
    }

    return samples;
}
