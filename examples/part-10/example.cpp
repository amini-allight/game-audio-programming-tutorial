#include "wav.hpp"
#include "tools.hpp"
#include "linear_algebra.hpp"
#include "pass_filters.hpp"
#include "convolution.hpp"

#include <thread>
#include <iostream>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

using namespace std;

constexpr mat4 listenerTransform = mat4();
constexpr vec3 listenerVelocity = vec3();
constexpr vec2 rightDir = vec2(1, 0);
constexpr vec3 localEarPositions[2]{
    vec3(-0.075, 0, 0),
    vec3(+0.075, 0, 0)
};

static vector<int16_t> audio;
static size_t readHead = 0;
static vec3 sourcePosition = vec3(0, 0.1, 0);

static vector<float> coefficients;

static void onOutput(void* userdata, uint8_t* output, int outputSize)
{
    assert(outputSize == systemBufferSize * systemChannels * sizeof(int16_t));

    vector<int16_t> monoDiscrete(overlappedBufferSize, 0);

    // All sizes are in bytes
    auto input = reinterpret_cast<const uint8_t*>(audio.data());
    int inputSize = audio.size() * sizeof(int16_t);
    int remainingSize = inputSize - readHead;
    int requestedSize = overlappedBufferSize * sizeof(int16_t);
    int readSize = min(requestedSize, remainingSize);
    memcpy(monoDiscrete.data(), input + readHead, readSize);
    readHead += systemBufferSize * sizeof(int16_t);

    vector<float> mono = discreteToContinuous(monoDiscrete);

    mono = convolve(mono, coefficients);

    vec3 localSourcePosition = listenerTransform.toLocal(sourcePosition);

    vector<float> ears[systemChannels];

    for (size_t ear = 0; ear < systemChannels; ear++)
    {
        ears[ear] = mono;

        vec3 localEarPosition = localEarPositions[ear];

        float distance = localEarPosition.distanceTo(localSourcePosition);
        vec3 direction = localEarPosition.directionTo(localSourcePosition);

        float falloff = 1 / pow(1 + distance, 2);
        float angle = angleBetween(direction.toVec2().normalize(), rightDir) / 2;
        float panning = ear == 0 ? sin(angle) : cos(angle);

        for (size_t i = 0; i < overlappedBufferSize; i++)
        {
            ears[ear][i] = panning * ears[ear][i] * falloff;
        }
    }

    vector<float> stereo = joinStereo(
        subBuffer(ears[0], overlapSize, systemBufferSize),
        subBuffer(ears[1], overlapSize, systemBufferSize)
    );

    vector<int16_t> stereoDiscrete = continuousToDiscrete(stereo);

    memcpy(output, stereoDiscrete.data(), stereoDiscrete.size() * sizeof(int16_t));
}

int main(int, char**)
{
    int error = SDL_Init(SDL_INIT_AUDIO);

    if (error)
    {
        cerr << "Failed to initialize SDL audio: " << SDL_GetError() << endl;
        return 1;
    }

    SDL_AudioSpec outputDesired{};
    outputDesired.freq = systemFrequency;
    outputDesired.format = AUDIO_S16SYS;
    outputDesired.channels = systemChannels;
    outputDesired.samples = systemBufferSize;
    outputDesired.callback = onOutput;

    SDL_AudioSpec outputObtained{};

    int outputDevice = SDL_OpenAudioDevice(
        nullptr,
        false,
        &outputDesired,
        &outputObtained,
        0
    );;

    if (!outputDevice)
    {
        cerr << "Failed to open audio output device: " << SDL_GetError() << endl;
        return 1;
    }

    coefficients = hammingWindowedLoPass(1000, 2000);

    WAVContents contents = loadWAV("../../res/music-mono.wav");

    audio = contents.samples;

    chrono::microseconds startTime = currentTime();
    SDL_PauseAudioDevice(outputDevice, false);

    this_thread::sleep_for(chrono::seconds(30));

    SDL_CloseAudioDevice(outputDevice);

    SDL_Quit();

    return 0;
}
