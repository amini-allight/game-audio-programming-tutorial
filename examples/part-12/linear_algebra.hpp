#pragma once

#include "constants.hpp"

#include <cmath>
#include <ostream>

using namespace std;

struct vec2
{
    constexpr vec2()
        : x(0)
        , y(0)
    {

    }

    explicit constexpr vec2(float v)
        : x(v)
        , y(v)
    {

    }

    constexpr vec2(float x, float y)
        : x(x)
        , y(y)
    {

    }

    constexpr float dot(const vec2& rhs) const
    {
        return x*rhs.x + y*rhs.y;
    }

    constexpr vec2 normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        return *this / length();
    }

    constexpr float length() const
    {
        return sqrt(pow(x, 2) + pow(y, 2));
    }

    constexpr vec2 directionTo(const vec2& rhs) const
    {
        return (rhs - *this).normalize();
    }

    constexpr float distanceTo(const vec2& rhs) const
    {
        return (rhs - *this).length();
    }

    constexpr vec2 operator+() const
    {
        return *this;
    }

    constexpr vec2 operator-() const
    {
        return vec2(
            -x,
            -y
        );
    }

    constexpr vec2 operator+(const vec2& rhs) const
    {
        return vec2(
            x + rhs.x,
            y + rhs.y
        );
    }

    constexpr vec2 operator-(const vec2& rhs) const
    {
        return vec2(
            x - rhs.x,
            y - rhs.y
        );
    }

    constexpr vec2 operator*(const vec2& rhs) const
    {
        return vec2(
            x * rhs.x,
            y * rhs.y
        );
    }

    constexpr vec2 operator/(const vec2& rhs) const
    {
        return vec2(
            x / rhs.x,
            y / rhs.y
        );
    }

    constexpr vec2& operator+=(const vec2& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    constexpr vec2& operator-=(const vec2& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    constexpr vec2& operator*=(const vec2& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr vec2& operator/=(const vec2& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr vec2 operator+(float rhs) const
    {
        return *this + vec2(rhs);
    }

    constexpr vec2 operator-(float rhs) const
    {
        return *this - vec2(rhs);
    }

    constexpr vec2 operator*(float rhs) const
    {
        return *this * vec2(rhs);
    }

    constexpr vec2 operator/(float rhs) const
    {
        return *this / vec2(rhs);
    }

    constexpr vec2& operator+=(float rhs)
    {
        *this = *this + vec2(rhs);

        return *this;
    }

    constexpr vec2& operator-=(float rhs)
    {
        *this = *this - vec2(rhs);

        return *this;
    }

    constexpr vec2& operator*=(float rhs)
    {
        *this = *this * vec2(rhs);

        return *this;
    }

    constexpr vec2& operator/=(float rhs)
    {
        *this = *this / vec2(rhs);

        return *this;
    }

    constexpr bool operator==(const vec2& rhs)
    {
        return x == rhs.x &&
            y == rhs.y;
    }

    constexpr bool operator!=(const vec2& rhs)
    {
        return !(*this == rhs);
    }

    float x;
    float y;
};

inline ostream& operator<<(ostream& out, const vec2& v)
{
    out << "(" << v.x << ", " << v.y << ")";

    return out;
}

constexpr vec2 operator+(float lhs, const vec2& rhs)
{
    return vec2(lhs) + rhs;
}

constexpr vec2 operator-(float lhs, const vec2& rhs)
{
    return vec2(lhs) - rhs;
}

constexpr vec2 operator*(float lhs, const vec2& rhs)
{
    return vec2(lhs) * rhs;
}

constexpr vec2 operator/(float lhs, const vec2& rhs)
{
    return vec2(lhs) / rhs;
}

struct vec3
{
    constexpr vec3()
        : x(0)
        , y(0)
        , z(0)
    {

    }

    explicit constexpr vec3(float v)
        : x(v)
        , y(v)
        , z(v)
    {

    }

    constexpr vec3(float x, float y, float z)
        : x(x)
        , y(y)
        , z(z)
    {

    }

    constexpr vec3(const vec2& xy, float z)
        : x(xy.x)
        , y(xy.y)
        , z(z)
    {

    }

    constexpr float dot(const vec3& rhs) const
    {
        return x*rhs.x + y*rhs.y + z*rhs.z;
    }

    constexpr vec3 cross(const vec3& rhs) const
    {
        return vec3(
            y*rhs.z - rhs.y*z,
            z*rhs.x - rhs.z*x,
            x*rhs.y - rhs.x*y
        );
    }

    constexpr vec3 normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        return *this / length();
    }

    constexpr float length() const
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

    constexpr vec3 directionTo(const vec3& rhs) const
    {
        return (rhs - *this).normalize();
    }

    constexpr float distanceTo(const vec3& rhs) const
    {
        return (rhs - *this).length();
    }

    constexpr vec3 operator+() const
    {
        return *this;
    }

    constexpr vec3 operator-() const
    {
        return vec3(
            -x,
            -y,
            -z
        );
    }

    constexpr vec3 operator+(const vec3& rhs) const
    {
        return vec3(
            x + rhs.x,
            y + rhs.y,
            z + rhs.z
        );
    }

    constexpr vec3 operator-(const vec3& rhs) const
    {
        return vec3(
            x - rhs.x,
            y - rhs.y,
            z - rhs.z
        );
    }

    constexpr vec3 operator*(const vec3& rhs) const
    {
        return vec3(
            x * rhs.x,
            y * rhs.y,
            z * rhs.z
        );
    }

    constexpr vec3 operator/(const vec3& rhs) const
    {
        return vec3(
            x / rhs.x,
            y / rhs.y,
            z / rhs.z
        );
    }

    constexpr vec3& operator+=(const vec3& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    constexpr vec3& operator-=(const vec3& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    constexpr vec3& operator*=(const vec3& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr vec3& operator/=(const vec3& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr vec3 operator+(float rhs) const
    {
        return *this + vec3(rhs);
    }

    constexpr vec3 operator-(float rhs) const
    {
        return *this - vec3(rhs);
    }

    constexpr vec3 operator*(float rhs) const
    {
        return *this * vec3(rhs);
    }

    constexpr vec3 operator/(float rhs) const
    {
        return *this / vec3(rhs);
    }

    constexpr vec3& operator+=(float rhs)
    {
        *this = *this + vec3(rhs);

        return *this;
    }

    constexpr vec3& operator-=(float rhs)
    {
        *this = *this - vec3(rhs);

        return *this;
    }

    constexpr vec3& operator*=(float rhs)
    {
        *this = *this * vec3(rhs);

        return *this;
    }

    constexpr vec3& operator/=(float rhs)
    {
        *this = *this / vec3(rhs);

        return *this;
    }

    constexpr bool operator==(const vec3& rhs)
    {
        return x == rhs.x &&
            y == rhs.y &&
            z == rhs.z;
    }

    constexpr bool operator!=(const vec3& rhs)
    {
        return !(*this == rhs);
    }

    constexpr vec2 toVec2() const
    {
        return vec2(x, y);
    }

    float x;
    float y;
    float z;
};

inline ostream& operator<<(ostream& out, const vec3& v)
{
    out << "(" << v.x << ", " << v.y << ", " << v.z << ")";

    return out;
}

constexpr vec3 operator+(float lhs, const vec3& rhs)
{
    return vec3(lhs) + rhs;
}

constexpr vec3 operator-(float lhs, const vec3& rhs)
{
    return vec3(lhs) - rhs;
}

constexpr vec3 operator*(float lhs, const vec3& rhs)
{
    return vec3(lhs) * rhs;
}

constexpr vec3 operator/(float lhs, const vec3& rhs)
{
    return vec3(lhs) / rhs;
}

struct vec4
{
    constexpr vec4()
        : x(0)
        , y(0)
        , z(0)
        , w(0)
    {

    }

    explicit constexpr vec4(float v)
        : x(v)
        , y(v)
        , z(v)
        , w(v)
    {

    }

    constexpr vec4(float x, float y, float z, float w)
        : x(x)
        , y(y)
        , z(z)
        , w(w)
    {

    }

    constexpr vec4(const vec2& xy, float z, float w)
        : x(xy.x)
        , y(xy.y)
        , z(z)
        , w(w)
    {

    }

    constexpr vec4(const vec3& xyz, float w)
        : x(xyz.x)
        , y(xyz.y)
        , z(xyz.z)
        , w(w)
    {

    }

    constexpr float dot(const vec4& rhs) const
    {
        return x*rhs.x + y*rhs.y + z*rhs.z + w*rhs.w;
    }

    constexpr vec4 normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        return *this / length();
    }

    constexpr float length() const
    {
        return sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2) + pow(w, 2));
    }

    constexpr vec4 directionTo(const vec4& rhs) const
    {
        return (rhs - *this).normalize();
    }

    constexpr float distanceTo(const vec4& rhs) const
    {
        return (rhs - *this).length();
    }

    constexpr vec4 operator+() const
    {
        return *this;
    }

    constexpr vec4 operator-() const
    {
        return vec4(
            -x,
            -y,
            -z,
            -w
        );
    }

    constexpr vec4 operator+(const vec4& rhs) const
    {
        return vec4(
            x + rhs.x,
            y + rhs.y,
            z + rhs.z,
            w + rhs.w
        );
    }

    constexpr vec4 operator-(const vec4& rhs) const
    {
        return vec4(
            x - rhs.x,
            y - rhs.y,
            z - rhs.z,
            w - rhs.w
        );
    }

    constexpr vec4 operator*(const vec4& rhs) const
    {
        return vec4(
            x * rhs.x,
            y * rhs.y,
            z * rhs.z,
            w * rhs.w
        );
    }

    constexpr vec4 operator/(const vec4& rhs) const
    {
        return vec4(
            x / rhs.x,
            y / rhs.y,
            z / rhs.z,
            w / rhs.w
        );
    }

    constexpr vec4& operator+=(const vec4& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    constexpr vec4& operator-=(const vec4& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    constexpr vec4& operator*=(const vec4& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr vec4& operator/=(const vec4& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr vec4 operator+(float rhs) const
    {
        return *this + vec4(rhs);
    }

    constexpr vec4 operator-(float rhs) const
    {
        return *this - vec4(rhs);
    }

    constexpr vec4 operator*(float rhs) const
    {
        return *this * vec4(rhs);
    }

    constexpr vec4 operator/(float rhs) const
    {
        return *this / vec4(rhs);
    }

    constexpr vec4& operator+=(float rhs)
    {
        *this = *this + vec4(rhs);

        return *this;
    }

    constexpr vec4& operator-=(float rhs)
    {
        *this = *this - vec4(rhs);

        return *this;
    }

    constexpr vec4& operator*=(float rhs)
    {
        *this = *this * vec4(rhs);

        return *this;
    }

    constexpr vec4& operator/=(float rhs)
    {
        *this = *this / vec4(rhs);

        return *this;
    }

    constexpr bool operator==(const vec4& rhs)
    {
        return x == rhs.x &&
            y == rhs.y &&
            z == rhs.z &&
            w == rhs.w;
    }

    constexpr bool operator!=(const vec4& rhs)
    {
        return !(*this == rhs);
    }

    constexpr vec2 toVec2() const
    {
        return vec2(x, y);
    }

    constexpr vec3 toVec3() const
    {
        return vec3(x, y, z);
    }

    float x;
    float y;
    float z;
    float w;
};

inline ostream& operator<<(ostream& out, const vec4& v)
{
    out << "(" << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";

    return out;
}

constexpr vec4 operator+(float lhs, const vec4& rhs)
{
    return vec4(lhs) + rhs;
}

constexpr vec4 operator-(float lhs, const vec4& rhs)
{
    return vec4(lhs) - rhs;
}

constexpr vec4 operator*(float lhs, const vec4& rhs)
{
    return vec4(lhs) * rhs;
}

constexpr vec4 operator/(float lhs, const vec4& rhs)
{
    return vec4(lhs) / rhs;
}

struct quaternion
{
    constexpr quaternion()
        : w(1)
        , x(0)
        , y(0)
        , z(0)
    {

    }

    constexpr quaternion(float w, float x, float y, float z)
        : w(w)
        , x(x)
        , y(y)
        , z(z)
    {

    }

    constexpr quaternion(const vec3& axis, float angle)
        : w(cos(angle / 2))
        , x(axis.x * sin(angle / 2))
        , y(axis.y * sin(angle / 2))
        , z(axis.z * sin(angle / 2))
    {

    }

    constexpr float length() const
    {
        return sqrt(pow(w, 2) + pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

    constexpr quaternion normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        float l = length();

        return quaternion(
            w / l,
            x / l,
            y / l,
            z / l
        );
    }

    constexpr quaternion inverse() const
    {
        return quaternion(
            w,
            -x,
            -y,
            -z
        );
    }

    constexpr quaternion operator*(const quaternion& rhs) const
    {
        return quaternion(
            w*rhs.w - x*rhs.x - y*rhs.y - z*rhs.z,
            w*rhs.x + x*rhs.w + y*rhs.z - z*rhs.y,
            w*rhs.y - x*rhs.z + y*rhs.w + z*rhs.x,
            w*rhs.z + x*rhs.y - y*rhs.x + z*rhs.w
        );
    }

    constexpr quaternion operator/(const quaternion& rhs) const
    {
        return *this * rhs.inverse();
    }

    constexpr bool operator==(const quaternion& rhs) const
    {
        return w == rhs.w &&
            x == rhs.x &&
            y == rhs.y &&
            z == rhs.z;
    }

    constexpr bool operator!=(const quaternion& rhs) const
    {
        return !(*this == rhs);
    }


    float w;
    float x;
    float y;
    float z;
};

inline ostream& operator<<(ostream& out, const quaternion& q)
{
    out << "(" << q.w << ", " << q.x << ", " << q.y << ", " << q.z << ")";

    return out;
}

struct mat4
{
    constexpr mat4()
        : v{{0}}
    {
        v[0][0] = 1;
        v[1][1] = 1;
        v[2][2] = 1;
        v[3][3] = 1;
    }

    constexpr mat4(const vec3& position, const quaternion& rotation)
        : mat4()
    {
        float w = rotation.w;
        float x = rotation.x;
        float y = rotation.y;
        float z = rotation.z;

        vec3 rotX = vec3(1 - 2*y*y - 2*z*z, 2 * (x*y + w*z), 2 * (x*z - w*y)).normalize();
        vec3 rotY = vec3(2 * (x*y - w*z), 1 - 2*x*x - 2*z*z, 2 * (w*x + y*z)).normalize();
        vec3 rotZ = vec3(2 * (w*y + x*z), 2 * (y*z - w*x), 1 - 2*x*x - 2*y*y).normalize();

        v[0][0] = rotX.x;
        v[1][0] = rotY.x;
        v[2][0] = rotZ.x;

        v[0][1] = rotX.y;
        v[1][1] = rotY.y;
        v[2][1] = rotZ.y;

        v[0][2] = rotX.z;
        v[1][2] = rotY.z;
        v[2][2] = rotZ.z;

        v[3][0] = position.x;
        v[3][1] = position.y;
        v[3][2] = position.z;

        v[0][3] = 0;
        v[1][3] = 0;
        v[2][3] = 0;
        v[3][3] = 1;
    }

    constexpr mat4(
        float v00, float v01, float v02, float v03,
        float v10, float v11, float v12, float v13,
        float v20, float v21, float v22, float v23,
        float v30, float v31, float v32, float v33
    )
        : mat4()
    {
        v[0][0] = v00;
        v[0][1] = v01;
        v[0][2] = v02;
        v[0][3] = v03;

        v[1][0] = v10;
        v[1][1] = v11;
        v[1][2] = v12;
        v[1][3] = v13;

        v[2][0] = v20;
        v[2][1] = v21;
        v[2][2] = v22;
        v[2][3] = v23;

        v[3][0] = v30;
        v[3][1] = v31;
        v[3][2] = v32;
        v[3][3] = v33;
    }

    constexpr float determinant() const
    {
        float a2323 = v[2][2] * v[3][3] - v[2][3] * v[3][2];
        float a1323 = v[2][1] * v[3][3] - v[2][3] * v[3][1];
        float a1223 = v[2][1] * v[3][2] - v[2][2] * v[3][1];
        float a0323 = v[2][0] * v[3][3] - v[2][3] * v[3][0];
        float a0223 = v[2][0] * v[3][2] - v[2][2] * v[3][0];
        float a0123 = v[2][0] * v[3][1] - v[2][1] * v[3][0];

        return +v[0][0] * (v[1][1] * a2323 - v[1][2] * a1323 + v[1][3] * a1223)
               -v[0][1] * (v[1][0] * a2323 - v[1][2] * a0323 + v[1][3] * a0223)
               +v[0][2] * (v[1][0] * a1323 - v[1][1] * a0323 + v[1][3] * a0123)
               -v[0][3] * (v[1][0] * a1223 - v[1][1] * a0223 + v[1][2] * a0123);
    }

    constexpr mat4 inverse() const
    {
        float a2323 = v[2][2] * v[3][3] - v[2][3] * v[3][2];
        float a1323 = v[2][1] * v[3][3] - v[2][3] * v[3][1];
        float a1223 = v[2][1] * v[3][2] - v[2][2] * v[3][1];
        float a0323 = v[2][0] * v[3][3] - v[2][3] * v[3][0];
        float a0223 = v[2][0] * v[3][2] - v[2][2] * v[3][0];
        float a0123 = v[2][0] * v[3][1] - v[2][1] * v[3][0];
        float a2313 = v[1][2] * v[3][3] - v[1][3] * v[3][2];
        float a1313 = v[1][1] * v[3][3] - v[1][3] * v[3][1];
        float a1213 = v[1][1] * v[3][2] - v[1][2] * v[3][1];
        float a2312 = v[1][2] * v[2][3] - v[1][3] * v[2][2];
        float a1312 = v[1][1] * v[2][3] - v[1][3] * v[2][1];
        float a1212 = v[1][1] * v[2][2] - v[1][2] * v[2][1];
        float a0313 = v[1][0] * v[3][3] - v[1][3] * v[3][0];
        float a0213 = v[1][0] * v[3][2] - v[1][2] * v[3][0];
        float a0312 = v[1][0] * v[2][3] - v[1][3] * v[2][0];
        float a0212 = v[1][0] * v[2][2] - v[1][2] * v[2][0];
        float a0113 = v[1][0] * v[3][1] - v[1][1] * v[3][0];
        float a0112 = v[1][0] * v[2][1] - v[1][1] * v[2][0];

        float idet = 1 / determinant();

        return mat4(
           idet * +(v[1][1] * a2323 - v[1][2] * a1323 + v[1][3] * a1223),
           idet * -(v[0][1] * a2323 - v[0][2] * a1323 + v[0][3] * a1223),
           idet * +(v[0][1] * a2313 - v[0][2] * a1313 + v[0][3] * a1213),
           idet * -(v[0][1] * a2312 - v[0][2] * a1312 + v[0][3] * a1212),
           idet * -(v[1][0] * a2323 - v[1][2] * a0323 + v[1][3] * a0223),
           idet * +(v[0][0] * a2323 - v[0][2] * a0323 + v[0][3] * a0223),
           idet * -(v[0][0] * a2313 - v[0][2] * a0313 + v[0][3] * a0213),
           idet * +(v[0][0] * a2312 - v[0][2] * a0312 + v[0][3] * a0212),
           idet * +(v[1][0] * a1323 - v[1][1] * a0323 + v[1][3] * a0123),
           idet * -(v[0][0] * a1323 - v[0][1] * a0323 + v[0][3] * a0123),
           idet * +(v[0][0] * a1313 - v[0][1] * a0313 + v[0][3] * a0113),
           idet * -(v[0][0] * a1312 - v[0][1] * a0312 + v[0][3] * a0112),
           idet * -(v[1][0] * a1223 - v[1][1] * a0223 + v[1][2] * a0123),
           idet * +(v[0][0] * a1223 - v[0][1] * a0223 + v[0][2] * a0123),
           idet * -(v[0][0] * a1213 - v[0][1] * a0213 + v[0][2] * a0113),
           idet * +(v[0][0] * a1212 - v[0][1] * a0212 + v[0][2] * a0112)
        );
    }

    friend constexpr vec4 operator*(const mat4& lhs, const vec4& rhs);

    constexpr vec3 toLocal(const vec3& global) const
    {
        return (inverse() * vec4(global, 1)).toVec3();
    }

    constexpr vec3 toGlobal(const vec3& local) const
    {
        return (*this * vec4(local, 1)).toVec3();
    }

    constexpr mat4 operator*(const mat4& rhs) const
    {
        mat4 m;

        vec4 r0(rhs.v[0][0], rhs.v[0][1], rhs.v[0][2], rhs.v[0][3]);
        vec4 r1(rhs.v[1][0], rhs.v[1][1], rhs.v[1][2], rhs.v[1][3]);
        vec4 r2(rhs.v[2][0], rhs.v[2][1], rhs.v[2][2], rhs.v[2][3]);
        vec4 r3(rhs.v[3][0], rhs.v[3][1], rhs.v[3][2], rhs.v[3][3]);

        vec4 c0(v[0][0], v[1][0], v[2][0], v[3][0]);
        vec4 c1(v[0][1], v[1][1], v[2][1], v[3][1]);
        vec4 c2(v[0][2], v[1][2], v[2][2], v[3][2]);
        vec4 c3(v[0][3], v[1][3], v[2][3], v[3][3]);

        m.v[0][0] = r0.dot(c0);
        m.v[0][1] = r0.dot(c1);
        m.v[0][2] = r0.dot(c2);
        m.v[0][3] = r0.dot(c3);

        m.v[1][0] = r1.dot(c0);
        m.v[1][1] = r1.dot(c1);
        m.v[1][2] = r1.dot(c2);
        m.v[1][3] = r1.dot(c3);

        m.v[2][0] = r2.dot(c0);
        m.v[2][1] = r2.dot(c1);
        m.v[2][2] = r2.dot(c2);
        m.v[2][3] = r2.dot(c3);

        m.v[3][0] = r3.dot(c0);
        m.v[3][1] = r3.dot(c1);
        m.v[3][2] = r3.dot(c2);
        m.v[3][3] = r3.dot(c3);

        return m;
    }

    constexpr mat4& operator*=(const mat4& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr bool operator==(const mat4& rhs) const
    {
        for (size_t i = 0; i < 4; i++)
        {
            for (size_t j = 0; j < 4; j++)
            {
                if (v[i][j] != rhs.v[i][j])
                {
                    return false;
                }
            }
        }

        return true;
    }

    constexpr bool operator!=(const mat4& rhs) const
    {
        return !(*this == rhs);
    }

    float v[4][4];
};

inline ostream& operator<<(ostream& out, const mat4& m)
{
    out << "((" << m.v[0][0] << ", " << m.v[0][1] << ", " << m.v[0][2] << ", " << m.v[0][3] << "),\n";
    out << "(" << m.v[1][0] << ", " << m.v[1][1] << ", " << m.v[1][2] << ", " << m.v[1][3] << "),\n";
    out << "(" << m.v[2][0] << ", " << m.v[2][1] << ", " << m.v[2][2] << ", " << m.v[2][3] << "),\n";
    out << "(" << m.v[3][0] << ", " << m.v[3][1] << ", " << m.v[3][2] << ", " << m.v[3][3] << "))";

    return out;
}

constexpr vec4 operator*(const mat4& m, const vec4& v)
{
    vec4 c0(m.v[0][0], m.v[1][0], m.v[2][0], m.v[3][0]);
    vec4 c1(m.v[0][1], m.v[1][1], m.v[2][1], m.v[3][1]);
    vec4 c2(m.v[0][2], m.v[1][2], m.v[2][2], m.v[3][2]);
    vec4 c3(m.v[0][3], m.v[1][3], m.v[2][3], m.v[3][3]);

    return vec4(
        v.dot(c0),
        v.dot(c1),
        v.dot(c2),
        v.dot(c3)
    );
}

constexpr vec4 operator*(const vec4& v, const mat4& m)
{
    vec4 r0(m.v[0][0], m.v[0][1], m.v[0][2], m.v[0][3]);
    vec4 r1(m.v[1][0], m.v[1][1], m.v[1][2], m.v[1][3]);
    vec4 r2(m.v[2][0], m.v[2][1], m.v[2][2], m.v[2][3]);
    vec4 r3(m.v[3][0], m.v[3][1], m.v[3][2], m.v[3][3]);

    return vec4(
        v.dot(r0),
        v.dot(r1),
        v.dot(r2),
        v.dot(r3)
    );
}

constexpr vec2 rotate(vec2 point, float angle)
{
    float s = sin(-angle);
    float c = cos(-angle);

    float x = point.x * c - point.y * s;
    float y = point.x * s + point.y * c;

    return vec2(x, y);
}

constexpr float angleBetween(vec2 p, vec2 q)
{
    return acos(p.dot(q));
}

constexpr float radians(float degrees)
{
    return (degrees / 180) * pi;
}

constexpr float degrees(float radians)
{
    return (radians / pi) * 180;
}
