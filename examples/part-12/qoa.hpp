#pragma once

#include "constants.hpp"

#include <cassert>
#include <cstdint>
#include <cstring>
#include <vector>
#include <string>

#define QOA_IMPLEMENTATION
#include "qoa.h"

using namespace std;

struct QOAContents
{
    int channels;
    int frequency;
    vector<int16_t> samples;
};

inline QOAContents loadQOA(const string& path)
{
    qoa_desc description;

    auto rawSamples = static_cast<int16_t*>(qoa_read(path.c_str(), &description));

    assert(rawSamples);
    assert(description.channels == 1);
    assert(description.samplerate == systemFrequency);
    assert(description.samples > 0);

    vector<int16_t> samples(description.samples * description.channels, 0);

    memcpy(samples.data(), rawSamples, samples.size() * sizeof(int16_t));

    free(rawSamples);

    return {
        static_cast<int>(description.channels),
        static_cast<int>(description.samplerate),
        samples
    };
}
