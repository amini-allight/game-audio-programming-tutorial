#pragma once

#include "angular_frequency.hpp"

#include <cmath>

using namespace std;

inline size_t hammingWindowSize(float passFrequency, float stopFrequency)
{
    return (8 * pi) / toNormalizedAngularFrequency(abs(passFrequency - stopFrequency), systemFrequency);
}

inline float hammingWindow(size_t n, size_t m)
{
    return 0.54 - 0.46 * cos((2 * n * pi) / m);
}
