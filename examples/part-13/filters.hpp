#pragma once

#include "tools.hpp"
#include "angular_frequency.hpp"

using namespace std;

inline float loPassFilter(size_t n, size_t m, float passFrequency, float stopFrequency)
{
    float transitionCenterFrequency = toNormalizedAngularFrequency((passFrequency + stopFrequency) / 2, systemFrequency);

    return (transitionCenterFrequency / pi) * sinc((transitionCenterFrequency * (n - static_cast<float>(m / 2))) / pi);
}

inline float hiPassFilter(size_t n, size_t m, float passFrequency, float stopFrequency)
{
    float transitionCenterFrequency = toNormalizedAngularFrequency((passFrequency + stopFrequency) / 2, systemFrequency);

    return sinc(n - static_cast<float>(m / 2)) - (transitionCenterFrequency / pi) * sinc((transitionCenterFrequency * (n - static_cast<float>(m / 2))) / pi);
}
