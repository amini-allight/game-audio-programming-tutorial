#pragma once

#include "constants.hpp"

#include <cstdint>
#include <cmath>
#include <vector>

using namespace std;

inline vector<int16_t> generateTone(float frequency, float duration)
{
    int totalSamples = duration * systemFrequency;

    vector<int16_t> audio(totalSamples, 0);

    for (int i = 0; i < totalSamples; i++)
    {
        audio[i] = sin(frequency * tau * (i / static_cast<float>(systemFrequency))) * 32'767;
    }

    return audio;
}

inline vector<int16_t> generateSilence(float duration)
{
    int totalSamples = duration * systemFrequency;

    return vector<int16_t>(totalSamples, 0);
}
