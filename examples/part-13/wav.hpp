#pragma once

#include "constants.hpp"

#include <cstdint>
#include <cassert>
#include <cstdio>
#include <vector>
#include <string>

using namespace std;

#pragma pack(1)
struct RIFFHeader
{
    char headerID[4];
    int32_t totalFileSize;
    char fileType[4];
};

struct WAVEFormatHeader
{
    char headerID[4];
    int32_t sectionSize;
    int16_t audioFormat;
    int16_t channelCount;
    int32_t sampleRate;
    int32_t byteRate;
    int16_t blockAlign;
    int16_t bitsPerSample;
};

struct WAVEDataHeader
{
    char headerID[4];
    int32_t sectionSize;
};

struct GenericHeader
{
    char headerID[4];
    int32_t sectionSize;
};
#pragma pack()

struct WAVContents
{
    int channels;
    int frequency;
    vector<int16_t> samples;
};

inline WAVContents loadWAV(const string& path)
{
    size_t result;

    WAVContents contents{};

    FILE* file = fopen(path.c_str(), "rb");

    assert(file);

    // RIFF header
    RIFFHeader riffHeader{};
    result = fread(&riffHeader, sizeof(riffHeader), 1, file);

    assert(result == 1);
    assert(string(riffHeader.headerID, sizeof(riffHeader.headerID)) == "RIFF");
    assert(string(riffHeader.fileType, sizeof(riffHeader.fileType)) == "WAVE");

    while (true)
    {
        GenericHeader genericHeader{};
        result = fread(&genericHeader, sizeof(genericHeader), 1, file);

        if (result != 1)
        {
            break;
        }

        fseek(file, -sizeof(genericHeader), SEEK_CUR);

        string headerID(genericHeader.headerID, sizeof(genericHeader.headerID));

        // Format section
        if (headerID == "fmt ")
        {
            WAVEFormatHeader formatHeader{};
            result = fread(&formatHeader, sizeof(formatHeader), 1, file);

            assert(result == 1);
            assert(formatHeader.sampleRate == systemFrequency);
            assert(formatHeader.channelCount == 1);
            assert(formatHeader.bitsPerSample == 16);

            contents.frequency = formatHeader.sampleRate;
            contents.channels = formatHeader.channelCount;
        }
        // Data section
        else if (headerID == "data")
        {
            WAVEDataHeader dataHeader{};
            result = fread(&dataHeader, sizeof(dataHeader), 1, file);

            assert(result == 1);

            contents.samples = vector<int16_t>(dataHeader.sectionSize / sizeof(int16_t), 0);
            result = fread(contents.samples.data(), 1, dataHeader.sectionSize, file);

            assert(result == dataHeader.sectionSize);
        }
        else
        {
            fseek(file, sizeof(genericHeader) + genericHeader.sectionSize, SEEK_CUR);
        }
    }

    assert(contents.frequency != 0);
    assert(contents.channels != 0);
    assert(!contents.samples.empty());

    fclose(file);

    return contents;
}

inline void saveWAV(const string& path, const WAVContents& contents)
{
    FILE* file = fopen(path.c_str(), "wb");

    assert(file);

    RIFFHeader riffHeader{
        .headerID = { 'R', 'I', 'F', 'F' },
        .totalFileSize = static_cast<int32_t>(sizeof(RIFFHeader::fileType) + sizeof(WAVEFormatHeader) + sizeof(WAVEDataHeader) + contents.samples.size() * sizeof(int16_t)),
        .fileType = { 'W', 'A', 'V', 'E' }
    };

    fwrite(&riffHeader, sizeof(riffHeader), 1, file);

    WAVEFormatHeader formatHeader{
        .headerID = { 'f', 'm', 't', ' ' },
        .sectionSize = static_cast<int32_t>(sizeof(WAVEFormatHeader) - (sizeof(WAVEFormatHeader::headerID) + sizeof(WAVEFormatHeader::sectionSize))),
        .audioFormat = 1,
        .channelCount = static_cast<int16_t>(contents.channels),
        .sampleRate = contents.frequency,
        .byteRate = static_cast<int32_t>(contents.frequency * contents.channels * sizeof(int16_t)),
        .blockAlign = static_cast<int16_t>(contents.channels * sizeof(int16_t)),
        .bitsPerSample = sizeof(int16_t) * 8
    };

    fwrite(&formatHeader, sizeof(formatHeader), 1, file);

    WAVEDataHeader dataHeader{
        .headerID = { 'd', 'a', 't', 'a' },
        .sectionSize = static_cast<int32_t>(contents.samples.size() * sizeof(int16_t))
    };

    fwrite(&dataHeader, sizeof(dataHeader), 1, file);

    fwrite(contents.samples.data(), sizeof(int16_t), contents.samples.size(), file);

    fclose(file);
}
