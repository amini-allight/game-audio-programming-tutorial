#include <thread>
#include <iostream>
#include <vector>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

using namespace std;

static constexpr float pi = 3.14159;
static constexpr float tau = pi * 2;

static constexpr int systemFrequency = 48000; // Hz
static constexpr int systemChannels = 2;
static constexpr int systemBufferSize = 2048; // samples per channel

static vector<int16_t> audio;
static size_t readHead = 0;

static void onOutput(void* userdata, uint8_t* output, int outputSize)
{
    // All sizes expressed in bytes
    auto input = reinterpret_cast<const uint8_t*>(audio.data());
    int inputSize = audio.size() * sizeof(int16_t);
    int remainingSize = inputSize - readHead;

    int writtenSize = min(outputSize, remainingSize);

    memcpy(output, input + readHead, writtenSize);
    memset(output + writtenSize, 0, outputSize - writtenSize);
    readHead += writtenSize;
}

static vector<int16_t> generateTone(float frequency, float duration)
{
    int totalSamples = duration * systemFrequency;

    vector<int16_t> audio(totalSamples, 0);

    for (int i = 0; i < totalSamples; i++)
    {
        audio[i] = sin(frequency * tau * (i / static_cast<float>(systemFrequency))) * 32'767;
    }

    return audio;
}

static vector<int16_t> generateSilence(float duration)
{
    int totalSamples = duration * systemFrequency;

    return vector<int16_t>(totalSamples, 0);
}

template<typename T>
tuple<vector<T>, vector<int16_t>> splitStereo(const vector<T>& interleaved)
{
    vector<T> left(interleaved.size() / 2);
    vector<T> right(interleaved.size() / 2);

    for (size_t i = 0; i < left.size(); i++)
    {
        left[i] = interleaved[i * 2 + 0];
        right[i] = interleaved[i * 2 + 1];
    }

    return { left, right };
}

template<typename T>
vector<T> joinStereo(const vector<T>& left, const vector<T>& right)
{
    vector<T> audio(left.size() * 2);

    for (size_t i = 0; i < left.size(); i++)
    {
        audio[i * 2 + 0] = left[i];
        audio[i * 2 + 1] = right[i];
    }

    return audio;
}

int main(int, char**)
{
    int error = SDL_Init(SDL_INIT_AUDIO);

    if (error)
    {
        cerr << "Failed to initialize SDL audio: " << SDL_GetError() << endl;
        return 1;
    }

    SDL_AudioSpec outputDesired{};
    outputDesired.freq = systemFrequency;
    outputDesired.format = AUDIO_S16SYS;
    outputDesired.channels = systemChannels;
    outputDesired.samples = systemBufferSize;
    outputDesired.callback = onOutput;

    SDL_AudioSpec outputObtained{};

    int outputDevice = SDL_OpenAudioDevice(
        nullptr,
        false,
        &outputDesired,
        &outputObtained,
        0
    );;

    if (!outputDevice)
    {
        cerr << "Failed to open audio output device: " << SDL_GetError() << endl;
        return 1;
    }

    audio = joinStereo(generateTone(440, 5), generateSilence(5));
    SDL_PauseAudioDevice(outputDevice, false);
    this_thread::sleep_for(chrono::seconds(5));

    SDL_CloseAudioDevice(outputDevice);

    SDL_Quit();

    return 0;
}
