#pragma once

#include <vector>

using namespace std;

inline vector<float> convolve(const vector<float>& input, const vector<float>& coefficients)
{
    vector<float> output(input.size(), 0);

    for (int n = 0; n < output.size(); n++)
    {
        for (int m = min<int>(n, coefficients.size() - 1); m >= 0; m--)
        {
            output[n] += input[n - m] * coefficients[m];
        }
    }

    return output;
}
