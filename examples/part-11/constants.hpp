#pragma once

static constexpr float pi = 3.14159;
static constexpr float tau = pi * 2;

static constexpr int overlappedBufferSize = 1024;
static constexpr int overlapSize = 256;

static constexpr int systemFrequency = 48000; // Hz
static constexpr int systemChannels = 2;
static constexpr int systemBufferSize = overlappedBufferSize - overlapSize * 2; // samples per channel

static constexpr float speedOfSound = 343;
