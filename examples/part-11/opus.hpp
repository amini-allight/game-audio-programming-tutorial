#pragma once

#include "constants.hpp"

#include <cassert>
#include <cstdint>
#include <vector>
#include <string>

#include <opus/opusfile.h>

using namespace std;

struct OpusContents
{
    int channels;
    vector<int16_t> samples;
};

inline OpusContents loadOpus(const string& path)
{
    int result = 0;

    OggOpusFile* file = op_open_file(path.c_str(), &result);

    assert(result == 0);

    int channels = op_channel_count(file, 0);

    assert(channels == 1);

    int sampleCount = op_pcm_total(file, 0);

    assert(sampleCount > 0);

    vector<int16_t> samples(sampleCount, 0);

    if (channels == 2)
    {
        result = op_read_stereo(file, samples.data(), samples.size());
    }
    else
    {
        result = op_read(file, samples.data(), samples.size(), nullptr);
    }

    assert(result == sampleCount / channels);

    op_free(file);

    return {
        channels,
        samples
    };
}
