#pragma once

#include "tools.hpp"
#include "window_functions.hpp"
#include "filters.hpp"

using namespace std;

inline vector<float> hammingWindowedLoPass(float passFrequency, float stopFrequency)
{
    size_t m = hammingWindowSize(passFrequency, stopFrequency);

    vector<float> coefficients(m);

    for (size_t n = 0; n < m; n++)
    {
        coefficients[n] = hammingWindow(n, m) * loPassFilter(n, m, passFrequency, stopFrequency);
    }

    return coefficients;
}

inline vector<float> hammingWindowedHiPass(float passFrequency, float stopFrequency)
{
    size_t m = hammingWindowSize(passFrequency, stopFrequency);

    vector<float> coefficients(m);

    for (size_t n = 0; n < m; n++)
    {
        coefficients[n] = hammingWindow(n, m) * hiPassFilter(n, m, passFrequency, stopFrequency);
    }

    return coefficients;
}
