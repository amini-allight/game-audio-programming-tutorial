#pragma once

#include <cassert>
#include <cstdint>
#include <vector>

#include <opus/opus.h>

using namespace std;

class VoiceEncoder
{
public:
    VoiceEncoder(int channels, int frequency, int bitrate, float packetLoss)
        : channels(channels)
    {
        int status;

        encoder = opus_encoder_create(frequency, channels, OPUS_APPLICATION_VOIP, &status);
        assert(encoder);
        assert(status == OPUS_OK);

        status = opus_encoder_ctl(encoder, OPUS_SET_BITRATE(bitrate));
        assert(status == OPUS_OK);

        status = opus_encoder_ctl(encoder, OPUS_SET_PACKET_LOSS_PERC(packetLoss * 100));
        assert(status == OPUS_OK);

        status = opus_encoder_ctl(encoder, OPUS_SET_INBAND_FEC(packetLoss != 0));
        assert(status == OPUS_OK);
    }
    VoiceEncoder(const VoiceEncoder& rhs) = delete;
    VoiceEncoder(VoiceEncoder&& rhs) = delete;
    ~VoiceEncoder()
    {
        opus_encoder_destroy(encoder);
    }

    VoiceEncoder& operator=(const VoiceEncoder& rhs) = delete;
    VoiceEncoder& operator=(VoiceEncoder&& rhs) = delete;

    vector<uint8_t> encode(const vector<int16_t>& samples)
    {
        vector<uint8_t> encoded(samples.size() * sizeof(int16_t), 0);

        int status = opus_encode(
            encoder,
            samples.data(),
            samples.size() / channels,
            encoded.data(),
            encoded.size()
        );

        assert(status >= 0);

        return { encoded.begin(), encoded.begin() + status };
    }

private:
    int channels;
    OpusEncoder* encoder;
};

class VoiceDecoder
{
public:
    VoiceDecoder(int channels, int frequency)
        : channels(channels)
    {
        int status;

        decoder = opus_decoder_create(frequency, channels, &status);

        assert(decoder);
        assert(status == OPUS_OK);
    }
    VoiceDecoder(const VoiceDecoder& rhs) = delete;
    VoiceDecoder(VoiceDecoder&& rhs) = delete;
    ~VoiceDecoder()
    {
        opus_decoder_destroy(decoder);
    }

    VoiceDecoder& operator=(const VoiceDecoder& rhs) = delete;
    VoiceDecoder& operator=(VoiceDecoder&& rhs) = delete;

    vector<int16_t> decode(const vector<uint8_t>& encoded, bool recoverPacket)
    {
        int samplesPerChannel = opus_decoder_get_nb_samples(decoder, encoded.data(), encoded.size());

        assert(samplesPerChannel >= 0);

        vector<int16_t> samples(samplesPerChannel * channels, 0);

        int status = opus_decode(
            decoder,
            encoded.data(),
            encoded.size(),
            samples.data(),
            samples.size() / channels,
            recoverPacket
        );

        assert(status >= 0);

        return { samples.begin(), samples.begin() + status * channels };
    }

private:
    int channels;
    OpusDecoder* decoder;
};
