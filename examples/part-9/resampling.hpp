#pragma once

#include <vector>
#include <cmath>

using namespace std;

inline vector<float> linearResample(const vector<float>& input, float scale)
{
    vector<float> output(ceil(input.size() * scale), 0);

    for (size_t i = 0; i < output.size(); i++)
    {
        float elapsed = i / static_cast<float>(output.size());

        size_t aIndex = elapsed * input.size();
        size_t bIndex = aIndex + 1;

        if (bIndex >= input.size())
        {
            output[i] = input[aIndex];
        }
        else
        {
            float t = (elapsed - aIndex / static_cast<float>(input.size())) / (1.0f / input.size());

            output[i] = input[aIndex] * (1 - t) + input[bIndex] * t;
        }
    }

    return output;
}

inline vector<float> hermiteResample(const vector<float>& input, float scale)
{
    vector<float> output(ceil(input.size() * scale), 0);

    for (size_t i = 0; i < output.size(); i++)
    {
        float elapsed = i / static_cast<float>(output.size());

        ssize_t bIndex = elapsed * input.size();
        ssize_t aIndex = bIndex - 1;
        ssize_t cIndex = bIndex + 1;
        ssize_t dIndex = bIndex + 2;

        float a = aIndex >= 0 && aIndex < input.size() ? input[aIndex] : 0;
        float b = input[bIndex];
        float c = cIndex >= 0 && cIndex < input.size() ? input[cIndex] : 0;
        float d = dIndex >= 0 && dIndex < input.size() ? input[dIndex] : 0;

        float t = (elapsed * input.size()) - bIndex;

        float c0 = b;
        float c1 = (c - a) * 0.5f;
        float c2 = a - (b * 2.5f) + (c * 2.0f) - (d * 0.5f);
        float c3 = ((b - c) * 1.5f) + ((d - a) * 0.5f);
        output[i] = (((((c3 * t) + c2) * t) + c1) * t) + c0;
    }

    return output;
}

inline vector<float> sincResample(const vector<float>& input, float scale)
{
    vector<float> output(ceil(input.size() * scale), 0);

    // TODO: Not implemented

    return output;
}
