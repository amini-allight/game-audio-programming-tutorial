#pragma once

#include <cstdint>
#include <chrono>
#include <vector>
#include <algorithm>

using namespace std;

template<typename T>
tuple<vector<T>, vector<int16_t>> splitStereo(const vector<T>& interleaved)
{
    vector<T> left(interleaved.size() / 2);
    vector<T> right(interleaved.size() / 2);

    for (size_t i = 0; i < left.size(); i++)
    {
        left[i] = interleaved[i * 2 + 0];
        right[i] = interleaved[i * 2 + 1];
    }

    return { left, right };
}

template<typename T>
vector<T> joinStereo(const vector<T>& left, const vector<T>& right)
{
    vector<T> audio(left.size() * 2);

    for (size_t i = 0; i < left.size(); i++)
    {
        audio[i * 2 + 0] = left[i];
        audio[i * 2 + 1] = right[i];
    }

    return audio;
}

template<typename T>
vector<T> join(const vector<T>& head, const vector<T>& tail)
{
    vector<T> joined;

    joined.reserve(head.size() + tail.size());
    joined.insert(joined.end(), head.begin(), head.end());
    joined.insert(joined.end(), tail.begin(), tail.end());

    return joined;
}

template<typename T>
vector<T> subBuffer(const vector<T>& buffer, size_t start, size_t count)
{
    return {
        buffer.begin() + start,
        buffer.begin() + (start + count)
    };
}

inline chrono::microseconds currentTime()
{
    return chrono::duration_cast<chrono::microseconds>(chrono::system_clock::now().time_since_epoch());
}

inline int16_t continuousToDiscrete(float x)
{
    return x * 32'767;
}

inline float discreteToContinuous(int16_t x)
{
    return x / 32'768.0f;
}

inline vector<int16_t> continuousToDiscrete(const vector<float>& x)
{
    vector<int16_t> ret(x.size());

    transform(x.begin(), x.end(), ret.begin(), static_cast<int16_t(*)(float)>(continuousToDiscrete));

    return ret;
}

inline vector<float> discreteToContinuous(const vector<int16_t>& x)
{
    vector<float> ret(x.size());

    transform(x.begin(), x.end(), ret.begin(), static_cast<float(*)(int16_t)>(discreteToContinuous));

    return ret;
}

template<typename T>
bool isPowerOfTwo(T x)
{
    return x != 0 && (x & (x - 1)) == 0;
}
