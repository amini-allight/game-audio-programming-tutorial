#pragma once

#include "constants.hpp"
#include "fourier_transform.hpp"

#include <vector>
#include <complex>
#include <valarray>
#include <iostream>

using namespace std;

inline vector<float> dopplerShift(const vector<float>& samples, float doppler)
{
    valarray<complex<float>> frequencies = fft(samples);
    valarray<complex<float>> shiftedFrequencies(frequencies.size());

    size_t bucketCount = frequencies.size();

    for (size_t i = 0; i < bucketCount / 2; i++)
    {
        float inputFIndex = i * doppler;
        
        size_t beforeIndex = floor(inputFIndex);
        size_t afterIndex = ceil(inputFIndex);

        complex<float> beforeInput = beforeIndex >= 0 && beforeIndex < bucketCount / 2
            ? frequencies[beforeIndex]
            : 0;
        complex<float> afterInput = afterIndex >= 0 && afterIndex < bucketCount / 2
            ? frequencies[afterIndex]
            : 0;

        float t = inputFIndex - floor(inputFIndex);

        shiftedFrequencies[i] = beforeInput * (1 - t) + afterInput * t;
    }

    for (size_t i = 0; i < bucketCount / 2; i++)
    {
        float inputFIndex = i * doppler;
        
        size_t beforeIndex = bucketCount - (floor(inputFIndex) + 1);
        size_t afterIndex = bucketCount - (ceil(inputFIndex) + 1);

        complex<float> beforeInput = beforeIndex >= bucketCount / 2 && beforeIndex < bucketCount
            ? frequencies[beforeIndex]
            : 0;
        complex<float> afterInput = afterIndex >= bucketCount / 2 && afterIndex < bucketCount
            ? frequencies[afterIndex]
            : 0;

        float t = inputFIndex - floor(inputFIndex);

        shiftedFrequencies[bucketCount - (i + 1)] = beforeInput * (1 - t) + afterInput * t;
    }

    return ifft(shiftedFrequencies);
}
