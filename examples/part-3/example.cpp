#include <thread>
#include <iostream>
#include <vector>
#include <cstdio>
#include <cassert>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

using namespace std;

#pragma pack(1)
struct RIFFHeader
{
    char headerID[4];
    int32_t totalFileSize;
    char fileType[4];
};

struct WAVEFormatHeader
{
    char headerID[4];
    int32_t sectionSize;
    int16_t audioFormat;
    int16_t channelCount;
    int32_t sampleRate;
    int32_t byteRate;
    int16_t blockAlign;
    int16_t bitsPerSample;
};

struct WAVEDataHeader
{
    char headerID[4];
    int32_t sectionSize;
};

struct GenericHeader
{
    char headerID[4];
    int32_t sectionSize;
};
#pragma pack()

struct WAVContents
{
    int channels;
    int frequency;
    vector<int16_t> samples;
};

static constexpr float pi = 3.14159;
static constexpr float tau = pi * 2;

static constexpr int systemFrequency = 48000; // Hz
static constexpr int systemChannels = 2;
static constexpr int systemBufferSize = 2048; // samples per channel

static vector<int16_t> audio;
static size_t readHead = 0;

static void onOutput(void* userdata, uint8_t* output, int outputSize)
{
    // All sizes expressed in bytes
    auto input = reinterpret_cast<const uint8_t*>(audio.data());
    int inputSize = audio.size() * sizeof(int16_t);
    int remainingSize = inputSize - readHead;

    int writtenSize = min(outputSize, remainingSize);

    memcpy(output, input + readHead, writtenSize);
    memset(output + writtenSize, 0, outputSize - writtenSize);
    readHead += writtenSize;
}

static vector<int16_t> generateTone(float frequency, float duration)
{
    int totalSamples = duration * systemFrequency;

    vector<int16_t> audio(totalSamples, 0);

    for (int i = 0; i < totalSamples; i++)
    {
        audio[i] = sin(frequency * tau * (i / static_cast<float>(systemFrequency))) * 32'767;
    }

    return audio;
}

static vector<int16_t> generateSilence(float duration)
{
    int totalSamples = duration * systemFrequency;

    return vector<int16_t>(totalSamples, 0);
}

template<typename T>
tuple<vector<T>, vector<int16_t>> splitStereo(const vector<T>& interleaved)
{
    vector<T> left(interleaved.size() / 2);
    vector<T> right(interleaved.size() / 2);

    for (size_t i = 0; i < left.size(); i++)
    {
        left[i] = interleaved[i * 2 + 0];
        right[i] = interleaved[i * 2 + 1];
    }

    return { left, right };
}

template<typename T>
vector<T> joinStereo(const vector<T>& left, const vector<T>& right)
{
    vector<T> audio(left.size() * 2);

    for (size_t i = 0; i < left.size(); i++)
    {
        audio[i * 2 + 0] = left[i];
        audio[i * 2 + 1] = right[i];
    }

    return audio;
}

static WAVContents loadWAV(const string& path)
{
    size_t result;

    WAVContents contents{};

    FILE* file = fopen(path.c_str(), "rb");

    assert(file);

    // RIFF header
    RIFFHeader riffHeader{};
    result = fread(&riffHeader, sizeof(riffHeader), 1, file);

    assert(result == 1);
    assert(string(riffHeader.headerID, sizeof(riffHeader.headerID)) == "RIFF");
    assert(string(riffHeader.fileType, sizeof(riffHeader.fileType)) == "WAVE");

    while (true)
    {
        GenericHeader genericHeader{};
        result = fread(&genericHeader, sizeof(genericHeader), 1, file);

        if (result != 1)
        {
            break;
        }

        fseek(file, -sizeof(genericHeader), SEEK_CUR);

        string headerID(genericHeader.headerID, sizeof(genericHeader.headerID));

        // Format section
        if (headerID == "fmt ")
        {
            WAVEFormatHeader formatHeader{};
            result = fread(&formatHeader, sizeof(formatHeader), 1, file);

            assert(result == 1);
            assert(formatHeader.sampleRate == systemFrequency);
            assert(formatHeader.channelCount == systemChannels);
            assert(formatHeader.bitsPerSample == 16);

            contents.frequency = formatHeader.sampleRate;
            contents.channels = formatHeader.channelCount;
        }
        // Data section
        else if (headerID == "data")
        {
            WAVEDataHeader dataHeader{};
            result = fread(&dataHeader, sizeof(dataHeader), 1, file);

            assert(result == 1);

            contents.samples = vector<int16_t>(dataHeader.sectionSize / sizeof(int16_t), 0);
            result = fread(contents.samples.data(), 1, dataHeader.sectionSize, file);

            assert(result == dataHeader.sectionSize);
        }
        else
        {
            fseek(file, sizeof(genericHeader) + genericHeader.sectionSize, SEEK_CUR);
        }
    }

    assert(contents.frequency != 0);
    assert(contents.channels != 0);
    assert(!contents.samples.empty());

    fclose(file);

    return contents;
}

int main(int, char**)
{
    int error = SDL_Init(SDL_INIT_AUDIO);

    if (error)
    {
        cerr << "Failed to initialize SDL audio: " << SDL_GetError() << endl;
        return 1;
    }

    SDL_AudioSpec outputDesired{};
    outputDesired.freq = systemFrequency;
    outputDesired.format = AUDIO_S16SYS;
    outputDesired.channels = systemChannels;
    outputDesired.samples = systemBufferSize;
    outputDesired.callback = onOutput;

    SDL_AudioSpec outputObtained{};

    int outputDevice = SDL_OpenAudioDevice(
        nullptr,
        false,
        &outputDesired,
        &outputObtained,
        0
    );;

    if (!outputDevice)
    {
        cerr << "Failed to open audio output device: " << SDL_GetError() << endl;
        return 1;
    }

    WAVContents contents = loadWAV("../../res/music-stereo.wav");

    audio = contents.samples;

    SDL_PauseAudioDevice(outputDevice, false);
    this_thread::sleep_for(chrono::seconds(30));

    SDL_CloseAudioDevice(outputDevice);

    SDL_Quit();

    return 0;
}
