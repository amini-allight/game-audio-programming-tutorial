#include "generators.hpp"
#include "wav.hpp"
#include "tools.hpp"
#include "linear_algebra.hpp"
#include "doppler_shift.hpp"

#include <thread>
#include <iostream>

#define SDL_MAIN_HANDLED
#include <SDL2/SDL.h>

using namespace std;

constexpr vec2 sourceStartPosition = vec2(0.5, -10);
constexpr vec2 sourceVelocity = vec2(0, 2);
constexpr mat4 listenerTransform = mat4();
constexpr vec3 listenerVelocity = vec3();
constexpr vec2 rightDir = vec2(1, 0);
constexpr vec3 localEarPositions[2]{
    vec3(-0.075, 0, 0),
    vec3(+0.075, 0, 0)
};

static vector<int16_t> audio;
static size_t readHead = 0;
static vec3 sourcePosition = vec3(sourceStartPosition, 0);

static void onOutput(void* userdata, uint8_t* output, int outputSize)
{
    int requestedSamplesPerChannel = outputSize / systemChannels / sizeof(int16_t);

    vector<int16_t> monoDiscrete(requestedSamplesPerChannel, 0);

    // All sizes are in bytes
    auto input = reinterpret_cast<const uint8_t*>(audio.data());
    int inputSize = audio.size() * sizeof(int16_t);
    int remainingSize = inputSize - readHead;
    int requestedSize = outputSize / systemChannels;
    int readSize = min(requestedSize, remainingSize);
    memcpy(monoDiscrete.data(), input + readHead, readSize);
    readHead += readSize;

    vector<float> mono = discreteToContinuous(monoDiscrete);

    vec3 localSourcePosition = listenerTransform.toLocal(sourcePosition);

    vector<float> ears[systemChannels];

    for (size_t ear = 0; ear < systemChannels; ear++)
    {
        ears[ear] = mono;

        vec3 localEarPosition = localEarPositions[ear];

        float distance = localEarPosition.distanceTo(localSourcePosition);
        vec3 direction = localEarPosition.directionTo(localSourcePosition);

        vec3 relativeVelocity = vec3(sourceVelocity, 0) - listenerVelocity;
        float relativeSpeed = direction.dot(relativeVelocity);
        float doppler = speedOfSound / (speedOfSound - relativeSpeed);

        if (!isinf(doppler) && !isnan(doppler) && doppler > 0)
        {
            ears[ear] = dopplerShift(ears[ear], doppler);
        }
        else
        {
            ears[ear] = vector<float>(requestedSamplesPerChannel, 0);
        }

        float falloff = 1 / pow(1 + distance, 2);
        float angle = angleBetween(direction.toVec2().normalize(), rightDir) / 2;
        float panning = ear == 0 ? sin(angle) : cos(angle);

        for (size_t i = 0; i < requestedSamplesPerChannel; i++)
        {
            ears[ear][i] = panning * ears[ear][i] * falloff;
        }
    }

    vector<float> stereo = joinStereo(ears[0], ears[1]);

    vector<int16_t> stereoDiscrete = continuousToDiscrete(stereo);

    memcpy(output, stereoDiscrete.data(), stereoDiscrete.size() * sizeof(int16_t));
}

int main(int, char**)
{
    int error = SDL_Init(SDL_INIT_AUDIO);

    if (error)
    {
        cerr << "Failed to initialize SDL audio: " << SDL_GetError() << endl;
        return 1;
    }

    SDL_AudioSpec outputDesired{};
    outputDesired.freq = systemFrequency;
    outputDesired.format = AUDIO_S16SYS;
    outputDesired.channels = systemChannels;
    outputDesired.samples = systemBufferSize;
    outputDesired.callback = onOutput;

    SDL_AudioSpec outputObtained{};

    int outputDevice = SDL_OpenAudioDevice(
        nullptr,
        false,
        &outputDesired,
        &outputObtained,
        0
    );;

    if (!outputDevice)
    {
        cerr << "Failed to open audio output device: " << SDL_GetError() << endl;
        return 1;
    }

    audio = generateTone(440, 30);

    chrono::microseconds startTime = currentTime();
    SDL_PauseAudioDevice(outputDevice, false);

    while (currentTime() - startTime < chrono::seconds(30))
    {
        this_thread::sleep_for(chrono::milliseconds(20));

        float elapsed = (currentTime() - startTime).count() / 1'000'000.0f;

        sourcePosition = vec3(sourceStartPosition + sourceVelocity * elapsed, 0);
    }

    SDL_CloseAudioDevice(outputDevice);

    SDL_Quit();

    return 0;
}
