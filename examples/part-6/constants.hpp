#pragma once

static constexpr float pi = 3.14159;
static constexpr float tau = pi * 2;

static constexpr int systemFrequency = 48000; // Hz
static constexpr int systemChannels = 2;
static constexpr int systemBufferSize = 2048; // samples per channel
