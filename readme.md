# Game Audio Programming Tutorial

This repository contains the source code for my game audio programming tutorial.

## License

Created by Amini Allight. The contents of this repository, with exceptions, are licensed under Creative Commons Zero (CC0 1.0), placing them in the public domain.

This license does **not** apply to the file `qoa.h` which is taken from [Quite OK Audio Format](https://github.com/phoboslab/qoa) under the terms of its license and modified slightly to add explicit typecasts which enable it to compile as C++.

This license does **not** apply to the file `speech.wav` which is extracted from [this YouTube video](https://www.youtube.com/watch?v=cTDln2f0GvQ) and is licensed as specified [here](https://web.archive.org/web/20090429185439/http://change.gov/about/copyright_policy). I didn't pick this for political reasons. It is just remarkably hard to find audio of human speech that is both clear (any recording that is public domain due to its age is from 1923 or earlier and so has terrible audio quality by modern standards) and has a compatible permissive license (even Wikipedia article narrations have a ShareAlike license which is dubiously compatible with the CC0 used for this tutorial). This was literally the first thing I found that fit those two criteria.
